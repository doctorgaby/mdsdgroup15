package allTest;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.junit.Before;
import org.junit.Test;

import Hotel.Booking.Booker;
import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

/**
 * 
 * Test for UC 2.1.5 "Edit a booking"
 *
 */
public class EditABookingTest {

	Administrator administrator;
	Receptionist receptionist;
	String startDate;
	String endDate;
	String firstName;
	String lastName;
	EMap<RoomType,Integer> roomTypeAmounts;
	int bookingId;
	int emptyBookingId;
	RoomType roomType;
	SimpleDateFormat format;
	Date dateFrom;
	Date dateTo;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		receptionist = ReceptionistFactory.eINSTANCE.createReceptionist();
		startDate = "20150101";
		endDate = "20161220";
		firstName = "John";
		lastName = "Doe";
		roomTypeAmounts = new BasicEMap<RoomType,Integer>();
		roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Default Description");
		roomTypeAmounts.put(roomType, 5);
		bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		emptyBookingId = receptionist.initiateBooking("", startDate, endDate, lastName);
		
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
			}
		}
		
		receptionist.confirmBooking(bookingId);
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		assertTrue(booking.isConfirmed());
		
		format = new SimpleDateFormat("yyyyMMdd");
		
		try{
		    dateFrom = format.parse(startDate);
		    dateTo = format.parse(endDate);
			} catch (Exception e){
				fail("Exception when parsing date. Test failed!");
			}
		
	}
	

	/**
	 * Try to set an invalid time period for the booking, with end date before start date. 
	 * editBooking should fail.
	 */
	@Test
	public void testInvalidPeriod() {

		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		Booker booker = BookingInterfaceImpl.basicGetInstance().getBooker();
		
		EMap<RoomType,Integer> newRoomTypeAmounts = new BasicEMap<RoomType,Integer>();
		administrator.addRoomType("Edited room type", 1000, 2, null, "Edited room type");
		
		RoomType newRoomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Edited room type");
		newRoomTypeAmounts.put(newRoomType, 3);
		String newStartDate = "20150125";
		String newEndDate = "20150110";	
		
		try{
		Date newFrom = format.parse(newStartDate);
		Date newTo = format.parse(newEndDate);
		assertFalse(booker.editBooking(booking.getBookingId(), newRoomTypeAmounts, newFrom, newTo));		
		
		assertTrue(booking.getFromDay().equals(dateFrom));
		assertTrue(booking.getToDay().equals(dateTo));
		
		} catch (Exception e){
			fail("Exception when parsing date. Test failed!");
		}
		
	}
	
	/**
	 * Tries to change to a non existent room type. Should fail.
	 */
	@Test
	public void testEditToNonExistentRoomType() {
		
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		Booker booker = BookingInterfaceImpl.basicGetInstance().getBooker();
		
		assertFalse(receptionist.editBooking(bookingId, null, dateFrom, dateTo));

	}
	
	/**
	 * should give booking a new map of the new room type "Edited room type" times 3.
	 * should also set a new from date and to date.
	 */
	@Test
	public void shouldEditBooking() {
		
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		Booker booker = BookingInterfaceImpl.basicGetInstance().getBooker();
		
		EMap<RoomType,Integer> newRoomTypeAmounts = new BasicEMap<RoomType,Integer>();
		administrator.addRoomType("Edited room type", 1000, 2, null, "Edited room type");
		
		RoomType newRoomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Edited room type");
		newRoomTypeAmounts.put(newRoomType, 3);
		String newStartDate = "20150110";
		String newEndDate = "20150125";	
		
		try{
		Date newFrom = format.parse(newStartDate);
		Date newTo = format.parse(newEndDate);
		booker.editBooking(booking.getBookingId(), newRoomTypeAmounts, newFrom, newTo);		
		
		assertTrue(booking.getFromDay().equals(newFrom));
		assertTrue(booking.getToDay().equals(newTo));
		
		} catch (Exception e){
			fail("Exception when parsing date. Test failed!");
		}
		
		assertTrue(booking.getRoomTypeToInteger().equals(newRoomTypeAmounts));
	}

}
