package allTest;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.junit.Before;
import org.junit.Test;

import Hotel.Room.Room;
import Hotel.RoomType.RoomType;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

/**
 * Test class for use case 2.1.2 - Search for free rooms
 * 
 * @author Christoffer
 */
public class SearchFreeRoomsTest {
	
	Administrator administrator;
	Receptionist receptionist;
	String startDate;
	String endDate;
	String firstName;
	String lastName;
	Map<RoomType,Integer> roomTypeAmounts;
	int bookingId;
	RoomType roomType;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		receptionist = ReceptionistFactory.eINSTANCE.createReceptionist();
		startDate = "20150101";
		endDate = "20161220";
	}

	@Test
	public void searchFreeRooms(){
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date from = format.parse(startDate);
			Date to = format.parse(endDate);
			EMap<RoomType, EList<Room>> freeRoomInfo = receptionist.searchFreeRooms(from, to, 2);
			for(Map.Entry<RoomType, EList<Room>> entry : freeRoomInfo){
				System.out.print("[" + entry.getKey().getDescription() + "] [Price per room: " + entry.getKey().getPrice() + "] [Number of free rooms: " + entry.getValue().size() + "] [Number of beds: " + entry.getKey().getNumberOfBeds() + "]");
			}
			assertTrue(freeRoomInfo.size() > 0);
		} catch (Exception e){
			fail("Exception when parsing date. Test failed!");
		}
	}
	
	/**
	 * Alternative flow 2.1 for UC 2.1.2 - Search For Free Rooms
	 * Start date is not before end date.
	 */
	@Test
	public void searchFreeRoomsStartAfterEnd(){
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date from = format.parse(startDate);
			Date to = format.parse(endDate);
			EMap<RoomType, EList<Room>> freeRoomInfo = receptionist.searchFreeRooms(to, from, 2); //Order of parameters switched
			assertTrue(freeRoomInfo.isEmpty()); //Make sure the list is empty
		} catch (Exception e){
			fail("Exception when parsing date. Test failed!");
		}
	}

}
