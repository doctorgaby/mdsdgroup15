package allTest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Room.Room;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;

/**
 * Test class for use case 2.2.9 - Startup hotel system
 * 
 * @author Christoffer
 */
public class StartupHotelSystemTest {

	Administrator administrator;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);	//Creates 10 initial rooms
	}
	
	/**
	 * Checks that any previous bookings have been cleared.
	 */
	@Test
	public void startUpHotelSystemBookingsCleared(){
		List<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
		assertTrue(bookings.isEmpty());
	}
	
	/**
	 * Checks that any previous room types have been cleared.
	 * Only one room type should exist.
	 */
	@Test
	public void startUpHotelSystemRoomTypesCleared(){
		List<RoomType> roomTypes = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypes();
		assertTrue(roomTypes.size()==1);
	}
	
	/**
	 * Checks that all newly created rooms have exactly two beds.
	 */
	@Test
	public void startUpHotelSystemTwoBeds(){
		List<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
		for(Room room : rooms){
			assertFalse(room.getRoomType().getNumberOfBeds() != 2);
		}
	}
	
	/**
	 * Checks that the number of newly created rooms is correct (10).
	 * Any previous rooms must have been cleared for this to pass.
	 */
	@Test
	public void startUpHotelSystemNumberOfRooms() {
		List<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
		assertTrue(rooms.size()==10);
	}
	
	/**
	 * Checks that the newly created rooms are free.
	 */
	@Test
	public void startUpHotelSystemRoomsFree(){
		List<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
		for(Room room : rooms){
			assertFalse(!room.isFree());
		}
	}
	
	/**
	 * Checks that the newly created rooms are unpaid.
	 */
	@Test
	public void startUpHotelSystemRoomsUnpaid(){
		List<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
		for(Room room : rooms){
			assertFalse(!room.isUnpaid());
		}
	}

}
