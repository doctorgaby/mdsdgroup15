package allTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Hotel.Room.Room;
import Hotel.Room.RoomStatus;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;

/**
 * 
 * Test class for UC 2.2.8 "Unblock room"
 *
 */
public class UnblockRoomTest {

	Administrator administrator;
	int roomId;
	String roomTypeDescription;
	
	@Before
	public void setUp() throws Exception {
		
		roomTypeDescription = "Single bed";
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		roomId = 0;
	}

	/**
	 * Tries to block a non existent room, which should fail.
	 */
	@Test
	public void unblockShouldFail() {
		
		int nonExistentRoomId = 11;
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(nonExistentRoomId);
		assertFalse(administrator.blockRoom(nonExistentRoomId));
		
	}
	
	/**
	 * Tries to unblock an unblocked room which should fail.
	 */
	@Test
	public void testAlreadyUnblockedRoom() {
		
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomId);
		
		assertFalse(administrator.unblockRoom(roomId));

	}
	
	/**
	 * Blocks, then tries to unblock a room which should succeed.
	 */
	@Test
	public void testUnblockRoom() {
		
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomId);
		administrator.blockRoom(roomId);
		assertTrue(room.getRoomStatus() == RoomStatus.BLOCKED);
		
		administrator.unblockRoom(roomId);
		assertFalse(room.getRoomStatus() == RoomStatus.BLOCKED);

	}

}
