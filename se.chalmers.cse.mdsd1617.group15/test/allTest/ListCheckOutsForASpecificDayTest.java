package allTest;

import static org.junit.Assert.fail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import Hotel.Booking.Booking;
import Hotel.Entities.Extra;
import Hotel.RoomType.RoomType;
import Hotel.User.Administrator.impl.AdministratorImpl;
import Hotel.User.Receptionist.impl.ReceptionistImpl;

public class ListCheckOutsForASpecificDayTest {

	ReceptionistImpl receptionist = new ReceptionistImpl();
	AdministratorImpl administrator = new AdministratorImpl();
	
	@Before
	public void setUp() throws Exception {
		administrator.startUpHotelSystem(0);
		// Add a room with a roomType
		RoomType roomType = administrator.addRoomType("TestType", 10, 2, new BasicEList<Extra>(), "TestType");
		
		// Put the rooms in a list(for makeBooking)
		EMap<RoomType, Integer> rooms = new BasicEMap<RoomType, Integer>();
		rooms.put(roomType, 2);
		
		// Try to parse the dates and makeBooking
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date start = format.parse("20161219");
			Date end = format.parse("20161223");
			receptionist.makeBooking("Hampus", "Gunnrup", rooms, start, end);
			
			start = format.parse("20161224");
			end = format.parse("20161225");
			receptionist.makeBooking("Hampus", "Gunnrup", rooms, start, end);
		} catch(ParseException e) {
			
		}
	}
	
	@Test
	public void testNormal() {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date start = format.parse("20161222");
			Date end = format.parse("20161224");
			EList<Booking> checkOuts = receptionist.listCheckOuts(start, end);
			Assert.assertTrue(checkOuts.size() == 1);
		} catch(ParseException e) {
			fail("Parsing error @ AddExtraCostToRooms test");
		}
	}
	
	@Test
	public void testWrongPeriod() {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date start = format.parse("20161120");
			Date end = format.parse("20161120");
			EList<Booking> checkOuts = receptionist.listCheckOuts(start, end);
			Assert.assertTrue(checkOuts.size() == 0);
		} catch(ParseException e) {
			fail("Could not parse dates @testNormal()");
		}
	}

}
