package allTest;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.junit.Before;
import org.junit.Test;

import Hotel.Booking.Booking;
import Hotel.Booking.BookingStatus;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Room.Room;
import Hotel.Room.RoomStatus;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

/**
 * Test class for use case 2.1.6 - "Cancel a booking"
 * 
 */
public class CancelABookingTest {

	Administrator administrator;
	Receptionist receptionist;
	String startDate;
	String endDate;
	String firstName;
	String lastName;
	EMap<RoomType,Integer> roomTypeAmounts;
	int bookingId;
	int emptyBookingId;
	RoomType roomType;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		receptionist = ReceptionistFactory.eINSTANCE.createReceptionist();
		startDate = "20150101";
		endDate = "20161220";
		firstName = "John";
		lastName = "Doe";
		roomTypeAmounts = new BasicEMap<RoomType,Integer>();
		roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Default Description");
		roomTypeAmounts.put(roomType, 5);
		bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		emptyBookingId = receptionist.initiateBooking("", startDate, endDate, lastName);
		
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
			}
		}
		
		receptionist.confirmBooking(bookingId);
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		assertTrue(booking.isConfirmed());
	}
	
	/**
	 * Tries to cancel a non-existent booking, should return false
	 */
	@Test
	public void testCancelNonExistentBooking(){
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId+1);
		assertFalse(receptionist.cancelBooking(bookingId+1));
		
	}
	
	/**
	 * Tries to cancel the booking. 
	 * Checks if booking has been cancelled and all of its rooms have gotten status FREE
	 */
	@Test
	public void bookingShouldBeCancelled(){
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		for (Room room : booking.getRooms()){
			assertTrue(room.isBooked());
		}
		
		assertTrue(receptionist.cancelBooking(bookingId));
		assertTrue(booking.isCancelled());
		
		for (Room room : booking.getRooms()){
			assertTrue(room.isFree());
		}
		
	}

}
