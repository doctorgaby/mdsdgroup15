package allTest;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.junit.Before;
import org.junit.Test;

import Hotel.Room.Room;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

/**
 * Test class for use case 2.2.6 - Remove room
 * 
 * @author Christoffer
 */
public class RemoveRoomTest {
	
	Administrator administrator;
	Receptionist receptionist;
	String startDate;
	String endDate;
	String firstName;
	String lastName;
	EMap<RoomType,Integer> roomTypeAmounts;
	int bookingId;
	RoomType roomType;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		receptionist = ReceptionistFactory.eINSTANCE.createReceptionist();
		startDate = "20150101";
		endDate = "20161220";
		firstName = "John";
		lastName = "Doe";
		roomTypeAmounts = new BasicEMap<RoomType,Integer>();
		roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Default Description");
		roomTypeAmounts.put(roomType, 5);
		
		//Initiate booking
		bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);

		//Book rooms
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
			}
		}
		
		//Confirm booking
		receptionist.confirmBooking(bookingId);
	}
	
	/**
	 * Checks that the number of rooms after removal of one room is one less than before.
	 */
	@Test
	public void removeRoomNotCheckedIn() {
		List<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
		int numberOfRoomsBefore = rooms.size();
		assertTrue(administrator.removeRoom(0));
		int numberOfRoomsAfter = rooms.size();
		assertTrue(numberOfRoomsBefore-numberOfRoomsAfter == 1);
	}
	
	/**
	 * Makes sure that the room is NOT removed if a booking is checked into that room.
	 */
	@Test
	public void removeRoomCheckedIn(){
		receptionist.checkInBooking(bookingId);	//Check in booking (with room 0)
		List<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
		int numberOfRoomsBefore = rooms.size();
		assertTrue(!administrator.removeRoom(0));
		int numberOfRoomsAfter = rooms.size();
		assertTrue(numberOfRoomsBefore==numberOfRoomsAfter);
	}
	
}
