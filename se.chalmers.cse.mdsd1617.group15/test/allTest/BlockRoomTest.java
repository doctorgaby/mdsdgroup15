package allTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Hotel.Room.Room;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;

/**
 * Test class for use case 2.2.7 - Block room
 * 
 * @author Christoffer
 */
public class BlockRoomTest {
	
	Administrator administrator;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
	}

	/**
	 * Make sure the blocked room is blocked.
	 */
	@Test
	public void blockRoomRoomBlocked() {
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(0);
		assertTrue(room.isFree()); //Room should be free, as it just was created
		administrator.blockRoom(0);
		assertTrue(room.isBlocked()); //Room should be blocked, as it was just blocked
	}
	
	/**
	 * Make sure blockRoom return false when trying to block a room that does not exist.
	 */
	@Test
	public void blockRoomWrongRoomId(){
		assertTrue(!administrator.blockRoom(1000000000));	//This room ID does not exist (should return false)
	}

}
