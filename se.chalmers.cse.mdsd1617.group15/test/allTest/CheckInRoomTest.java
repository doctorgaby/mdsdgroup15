package allTest;

import static org.junit.Assert.*;

import java.util.Map;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.junit.Before;
import org.junit.Test;

import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Room.Room;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

public class CheckInRoomTest {

	Administrator administrator;
	Receptionist receptionist;
	String startDate;
	String endDate;
	String firstName;
	String lastName;
	EMap<RoomType,Integer> roomTypeAmounts;
	int bookingId;
	RoomType roomType;
	String roomTypeDescription;
	String otherRoomTypeDescription;
	
	@Before
		public void setUp(){
			administrator = AdministratorFactory.eINSTANCE.createAdministrator();
			administrator.startUpHotelSystem(10);
			receptionist = ReceptionistFactory.eINSTANCE.createReceptionist();
			startDate = "20150101";
			endDate = "20161220";
			firstName = "John";
			lastName = "Doe";
			roomTypeAmounts = new BasicEMap<RoomType,Integer>();
			roomTypeDescription = "Default Description";
			otherRoomTypeDescription = "Another roomType";
			administrator.addRoomType(otherRoomTypeDescription, 900, 2, null, otherRoomTypeDescription);
			
			roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription(roomTypeDescription);
			roomTypeAmounts.put(roomType, 5);
			
			//Initiate booking
			bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
			
			//Book rooms
			for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
				for(int i = 0; i < entry.getValue(); i++){
					receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
				}
			}
			
			//Confirm booking
			receptionist.confirmBooking(bookingId);
	}

	
	/**
	 * Tries to check in a room on an invalid booking (bookingId 2), should fail and return -1.
	 */
	@Test
	public void testCheckInInvalidBooking() {

		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(2);
		
		assertTrue(booking == null);

		assertTrue(receptionist.checkInRoom(2, roomType.getDescription()) == -1);


	}
	
	/**
	 * Tries to check in a room with a nonexistent room type, should fail.
	 */
	@Test
	public void testCheckInNonExistentRoomType() {

		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		
		String invalidRoomTypeDescription = "Do not exist";
		assertTrue(receptionist.checkInRoom(2, invalidRoomTypeDescription) == -1);
	}
	
	/**
	 * Tries to add a roomType which is not held by any of the rooms in the booking. Should fail and return -1.
	 */
	@Test
	public void testCheckInUnBookedRoom(){
		
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		
		assertTrue(receptionist.checkInRoom(bookingId, otherRoomTypeDescription) == -1);
		
	}
	
	/**
	 * Tries to check in a room of a room type, after already having checked in all of that bookings rooms
	 * of that room type. Last try should fail and return -1.
	 */
	@Test
	public void testAllRoomsOfTypeAlreadyCheckedIn(){
		
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				assertTrue(receptionist.checkInRoom(bookingId, roomTypeDescription) != -1);
			}
		}
		
		assertTrue(receptionist.checkInRoom(bookingId, roomTypeDescription) == -1);
		
	}
	/**
	 * asserts that a booking contains at least one room with the room type "default description"
	 *  which is not yet checked in. Then tries to check in this room and assert that it is given the
	 *  status OCCUPIED.
	 */
	@Test
	public void shouldCheckIn() {

		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		
		if (booking != null){
			assertTrue(booking.isConfirmed());
			boolean bookedRoomWithTypeExists = false;
			
			for (Room r : booking.getRooms()){
				if (r.getRoomType().equals(roomType) && r.isBooked()){
					bookedRoomWithTypeExists = true;
				}
			}

			assertTrue(bookedRoomWithTypeExists);
			
			if (bookedRoomWithTypeExists){
				int roomId = receptionist.checkInRoom(bookingId, roomType.getDescription());
				Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomId);
				assertTrue(room.isOccupied());
			} else {
				fail("No booked room with the chosen room type exists");
			}
		}
	}

}
