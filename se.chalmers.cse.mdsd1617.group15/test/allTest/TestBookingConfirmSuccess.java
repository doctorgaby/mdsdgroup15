/**
 * 
 */
package allTest;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import Hotel.IHotelCustomerProvides;
import Hotel.IHotelStartupProvides;
import Hotel.impl.HotelFactoryImpl;

/**
 * @author hampus
 *
 */
public class TestBookingConfirmSuccess {
	IHotelStartupProvides startupHotel;
	IHotelCustomerProvides customerInterface;
	int bookingID;
	
	@Before
	public void setUp() throws Exception {
		startupHotel = HotelFactoryImpl.init().createIHotelStartupProvides();
		customerInterface = HotelFactoryImpl.init().createIHotelCustomerProvides();
		
		// startup hotel
		startupHotel.startup(5);
		// Initiate Booking
		String fromDate = "20161218";
		String toDate = "20161223";
		bookingID = customerInterface.initiateBooking("Hampus", fromDate, toDate, "Gunnrup");
		// Add a room
		customerInterface.addRoomToBooking(customerInterface.getFreeRooms(2, fromDate, toDate).get(0).getRoomTypeDescription(), bookingID);
	}

	@Test
	public void test() {
		assertTrue(customerInterface.confirmBooking(bookingID));
	}

}
