package allTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ListCheckInsForASpecificDayTest.class, ListCheckOutsForASpecificDayTest.class, TestBookingConfirmSuccess.class, AddExtraCostToRoomsTest.class, AddRoomTypeTest.class, CheckOutBookingTest.class})
public class AllHampus {
}
