package allTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Hotel.Room.Room;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

/**
 * 
 * Tests use case 2.2.4 "Add room"
 *
 */
public class AddRoomTest {

	Administrator administrator;
	int roomId;
	String roomTypeDescription;
	
	
	@Before
	public void setUp() throws Exception {
		
		roomTypeDescription = "Single bed";
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		administrator.addRoomType(roomTypeDescription, 600, 1, null, roomTypeDescription);
		RoomInterfaceImpl.basicGetInstance().clearComponent();
	}

	/**
	 * Tries to add a room with a non existent room type, should fail and return -1.
	 */
	@Test
	public void testAddRoomwithInvalidRoomType() {
		String invalidRoomDescription = "Do not exist";
		roomId = administrator.addRoom(0, invalidRoomDescription);
		assertTrue(roomId == -1);
	}
	
	/**
	 * Tries to add a room which should succeed, 
	 * then tries to add a room with the same room number, which should fail.
	 */
	@Test
	public void testAddSameRoomTwice() {
		
		roomId = administrator.addRoom(0, roomTypeDescription);
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomId);
		assertTrue(room.getRoomType().getDescription().equals(roomTypeDescription));
		
		roomId = administrator.addRoom(0, roomTypeDescription);
		room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomId);
		assertTrue(roomId == -1);
	}

}
