package allTest;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.junit.Before;
import org.junit.Test;

import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Room.Room;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

/**
 * Test class for use case 2.1.1 - Make a Booking
 * 
 * @author Christoffer
 */
public class MakeABookingTest {

	Administrator administrator;
	Receptionist receptionist;
	String startDate;
	String endDate;
	String firstName;
	String lastName;
	EMap<RoomType,Integer> roomTypeAmounts;
	int bookingId;
	int emptyBookingId;
	RoomType roomType;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		receptionist = ReceptionistFactory.eINSTANCE.createReceptionist();
		startDate = "20150101";
		endDate = "20161220";
		firstName = "John";
		lastName = "Doe";
		roomTypeAmounts = new BasicEMap<RoomType,Integer>();
		roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Default Description");
		roomTypeAmounts.put(roomType, 5);
		bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		emptyBookingId = receptionist.initiateBooking("", startDate, endDate, lastName);
	}
	
	/**
	 * Make sure the created booking has ID 1.
	 */
	@Test
	public void initiateBookingCorrectId() {
		assertEquals(bookingId,1);
	}
	
	/**
	 * Alternative flow 2.1 for UC 2.1.1:
	 * Make sure initiateBooking returns -1 if parameters are wrong.
	 */
	@Test
	public void initiateBookingEmptyId(){
		assertEquals(emptyBookingId,-1);
	}
	
	/**
	 * Make sure the created booking is marked as unpaid.
	 */
	@Test
	public void initiateBookingIsUnpaid(){
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		assertTrue(booking.isUnpaid());
	}
	
	/**
	 * Make sure the created booking is marked as unconfirmed.
	 */
	@Test
	public void initiateBookingIsUnconfirmed(){
		receptionist.confirmBooking(bookingId);
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		assertTrue(booking.isUnconfirmed());
	}
	
	/**
	 * Book some rooms.
	 * Make sure that addRoomToBooking always returns true;
	 */
	@Test
	public void addRoomToBooking(){
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				assertFalse(!receptionist.addRoomToBooking(roomType.getDescription(), bookingId));
			}
		}
	}
	
	/**
	 * Alternative flow 5.1 for UC 2.1.1:
	 * Try to book a room that is not available.
	 */
	@Test
	public void addRoomToBookingNoRoomsAvailable(){
		//Book all available rooms
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < 10; i++){
				receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
			}
		}
		//Try to book one more room (even though it is not available)
		assertTrue(!receptionist.addRoomToBooking(roomType.getDescription(), bookingId));
	}
	
	/**
	 * Alternative flow 5.1 for UC 2.1.1:
	 * Room type does not exist.
	 */
	@Test
	public void addRoomToBookingNonExistingRoomType(){
		assertTrue(!receptionist.addRoomToBooking("This room type does not exist", bookingId));
	}
	
	/**
	 * Book rooms.
	 * Make sure rooms are linked to booking and marked as booked.
	 */
	@Test
	public void addRoomToBookingRoomBooked(){
		//Book rooms
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
			}
		}
		
		//Make sure rooms are marked as booked
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		List<Room> rooms = booking.getRooms();
		for(Room room : rooms){
			assertFalse(!room.isBooked());
		}
	}
	
	/**
	 * Book rooms and confirm booking.
	 * Make sure booking is marked as confirmed.
	 */
	@Test
	public void confirmBookingBookingConfirmed(){
		//Book rooms
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
			}
		}
		
		//Confirm booking
		receptionist.confirmBooking(bookingId);
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		assertTrue(booking.isConfirmed());
	}

}
