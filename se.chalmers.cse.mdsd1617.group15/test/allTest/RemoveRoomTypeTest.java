package allTest;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;

/**
 * 
 * Tests use case 2.2.3 "Remove room type"
 *
 */
public class RemoveRoomTypeTest {

	Administrator administrator;
	
	@Before
	public void setUp() throws Exception {
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		
		administrator.addRoomType("Double room", 1000, 1, null, "Double room");
		administrator.addRoomType("Triple room", 2000, 3, null, "Triple room");
		administrator.addRoom(1, "Triple room");
		
	}

	/**
	 * tries to remove room Type with description "Triple room", which should fail since there exist a room of
	 * that type.
	 */
	@Test
	public void shouldNotRemoveRoomType() {
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Triple room");
		assertTrue(roomType != null);
		
		administrator.removeRoomType("Triple room");
		roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Triple room");
		assertTrue(roomType == null);
		
		
	}
	
	/**
	 * tries to remove room Type with description "Double room", which should succeed since there exist no rooms of
	 * that type.
	 * Then tries to remove it again, which should fail as it shouldn't exist anymore.
	 */
	@Test
	public void shouldRemoveRoomType() {
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Double room");
		assertTrue(roomType != null);
		
		administrator.removeRoomType("Double room");
		roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Double room");
		assertTrue(roomType == null);
		
		assertFalse(administrator.removeRoomType("Double room")); 
		
	}

}
