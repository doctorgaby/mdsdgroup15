package allTest;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.junit.Before;
import org.junit.Test;

import Hotel.Room.Room;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

/**
 * Test class for use case 2.1.8 - List occupied rooms for a specified day
 *
 * @author Christoffer
 */
public class ListOccupiedRoomsForSpecifiedDayTest {
	Administrator administrator;
	Receptionist receptionist;
	String startDate;
	String endDate;
	String firstName;
	String lastName;
	EMap<RoomType,Integer> roomTypeAmounts;
	int bookingId;
	RoomType roomType;
	EList<Room> occupiedRooms;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		receptionist = ReceptionistFactory.eINSTANCE.createReceptionist();
		startDate = "20150101";
		endDate = "20161220";
		firstName = "John";
		lastName = "Doe";
		roomTypeAmounts = new BasicEMap<RoomType,Integer>();
		roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Default Description");
		roomTypeAmounts.put(roomType, 5);
		bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		
		//Book rooms
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
			}
		}
		
		//Confirm booking
		receptionist.confirmBooking(bookingId);
		
		//Check in booking
		receptionist.checkInBooking(bookingId);
	}
	
	/**
	 * Checks that the number of occupied rooms is 5 for the first day of the booking.
	 * Edge case.
	 */
	@Test
	public void listOccupiedRoomsNumberOfRoomsFirstDay() {
		try {
			String dateString = "20150101"; //The first day of the booking
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date date;
			date = format.parse(dateString);
			occupiedRooms = receptionist.listOccupiedRooms(date);
			assertTrue(occupiedRooms.size() == 5);
		} catch (ParseException e) {
			fail("Could not parse date! Test failed.");
		}
	}
	
	/**
	 * Checks that the number of occupied rooms is 5 for the last day of the booking.
	 * Edge case.
	 */
	@Test
	public void listOccupiedRoomsNumberOfRoomsLastDay() {
		try {
			String dateString = "20161220"; //The last day of the booking
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date date;
			date = format.parse(dateString);
			occupiedRooms = receptionist.listOccupiedRooms(date);
			assertTrue(occupiedRooms.size() == 5);
		} catch (ParseException e) {
			fail("Could not parse date! Test failed.");
		}
	}
	
	/**
	 * Checks that the number of occupied rooms is zero the day before the start of the booking.
	 * Edge case.
	 */
	@Test
	public void listOccupiedRoomsDateBefore(){
		//List occupied rooms
		try {
			String dateString = "20141231"; //The day before the start date of the booking
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date date;
			date = format.parse(dateString);
			occupiedRooms = receptionist.listOccupiedRooms(date);
			assertTrue(occupiedRooms.isEmpty());
		} catch (ParseException e) {
			fail("Could not parse date! Test failed.");
		}
	}
	
	/**
	 * Checks that the number of occupied rooms is zero the day after the end of the booking.
	 * Edge case.
	 */
	@Test
	public void listOccupiedRoomsDateAfter(){
		try {
			String dateString = "20161221"; //The day after the end date of the booking
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date date;
			date = format.parse(dateString);
			occupiedRooms = receptionist.listOccupiedRooms(date);
			assertTrue(occupiedRooms.isEmpty());
		} catch (ParseException e) {
			fail("Could not parse date! Test failed.");
		}
	}
	
	/**
	 * Checks that the rooms returned from listOccupiedRooms are actually occupied for an arbitrary day in the middle of the booking.
	 */
	@Test
	public void listOccupiedRoomsRoomsOccupied(){
		try {
			String dateString = "20150205"; //An arbitrary date within the interval of the booking
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date date;
			date = format.parse(dateString);
			occupiedRooms = receptionist.listOccupiedRooms(date);
			for(Room room : occupiedRooms){
				assertFalse(!room.isOccupied());
			}
		} catch (ParseException e) {
			fail("Could not parse date! Test failed.");
		}
	}

}
