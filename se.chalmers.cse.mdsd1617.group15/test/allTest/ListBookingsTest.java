package allTest;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.junit.Before;
import org.junit.Test;

import Hotel.Booking.Booking;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

/**
 * Test class for use case 2.1.7 - Test bookings
 * 
 * @author Christoffer
 */
public class ListBookingsTest {

	Administrator administrator;
	Receptionist receptionist;
	String startDate;
	String endDate;
	String firstName;
	String lastName;
	EMap<RoomType,Integer> roomTypeAmounts;
	int bookingId;
	RoomType roomType;
	List<Booking> bookings;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		receptionist = ReceptionistFactory.eINSTANCE.createReceptionist();
		startDate = "20150101";
		endDate = "20161220";
		firstName = "John";
		lastName = "Doe";
		roomTypeAmounts = new BasicEMap<RoomType,Integer>();
		roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Default Description");
		roomTypeAmounts.put(roomType, 5);
		bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		
		//Book rooms
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
			}
		}
		
		//Confirm booking
		receptionist.confirmBooking(bookingId);
		
		//List bookings
		bookings = receptionist.listBookings();
	}
	
	/**
	 * Checks that the total number of confirmed bookings is 1;
	 */
	@Test
	public void listBookingsSizeOne() {
		assertTrue(bookings.size()==1);
	}
	
	/**
	 * Checks that the bookings returned from listBookings are confirmed.
	 */
	@Test
	public void listBookingsBookingsConfirmed(){
		for(Booking booking : bookings){
			assertFalse(!booking.isConfirmed());
		}
	}

}
