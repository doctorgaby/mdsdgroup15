package allTest;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.junit.Before;
import org.junit.Test;

import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Room.Room;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

/**
 * Test class for use case 2.1.3 - Check in booking
 * 
 * @author Christoffer
 */
public class CheckInBookingTest {
	
	Administrator administrator;
	Receptionist receptionist;
	String startDate;
	String endDate;
	String firstName;
	String lastName;
	EMap<RoomType,Integer> roomTypeAmounts;
	int bookingId;
	RoomType roomType;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		receptionist = ReceptionistFactory.eINSTANCE.createReceptionist();
		startDate = "20150101";
		endDate = "20161220";
		firstName = "John";
		lastName = "Doe";
		roomTypeAmounts = new BasicEMap<RoomType,Integer>();
		roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Default Description");
		roomTypeAmounts.put(roomType, 5);
		
		//Initiate booking
		bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		
		//Book rooms
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
			}
		}
		
		//Confirm booking
		receptionist.confirmBooking(bookingId);
	}

	/**
	 * Checks that the booking has some booked rooms before trying to check in.
	 */
	@Test
	public void availableRoomsPreCheckIn() {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		if(booking == null){
			fail("A booking with that ID does not exist. Test failed!");
		}
		List<Room> bookedRooms = BookingInterfaceImpl.basicGetInstance().getBooker().getBookedRooms(bookingId);
		System.out.println("Available rooms for Booking " + bookingId + " (" + booking + ")");
		for(Room room : bookedRooms){
			System.out.println(room);
		}
		assertTrue(!bookedRooms.isEmpty());
	}
	
	/**
	 * Checks that the status of the booking is set to checked in after checking in.
	 */
	@Test
	public void checkInBookingBookingCheckedIn() {
		//Check in booking
		receptionist.checkInBooking(bookingId);
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		if(booking == null){
			fail("A booking with that ID does not exist. Test failed!");
		}
		assertTrue(booking.isCheckedIn());
	}
	
	/**
	 * Checks that the rooms of the booking are set to occupied after checking in.
	 */
	@Test
	public void checkInBookingRoomsOccupied(){
		receptionist.checkInBooking(bookingId);
		List<Room> rooms = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId).getRooms();
		for(Room room : rooms){
			assertFalse(!room.isOccupied());
		}
	}

}
