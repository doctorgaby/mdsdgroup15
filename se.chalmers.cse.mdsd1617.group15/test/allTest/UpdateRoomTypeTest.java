package allTest;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.junit.Before;
import org.junit.Test;

import Hotel.Entities.EntitiesFactory;
import Hotel.Entities.Extra;
import Hotel.Entities.impl.ExtraImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Administrator.impl.AdministratorImpl;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;
import Hotel.User.Receptionist.impl.ReceptionistImpl;

/**
 * 
 * Tests use case 2.2.2 "Update room type"
 *
 */
public class UpdateRoomTypeTest {

	Administrator administrator;
	Extra extra;
	EList<Extra> extras = new BasicEList<Extra>();
	
	
	@Before
	public void setUp() throws Exception {
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		
		extra = EntitiesFactory.eINSTANCE.createExtra();
		extra.setDescription("An extra");
		extra.setPrice(100);
		extras.add(extra);
		
		administrator.addRoomType("Double room", 1000, 2, extras, "Double room");
		administrator.addRoomType("Triple room", 1500, 3, extras, "Triple room");
		
	}

	/**
	 * Tries to update a room type to the same name as another room type, which should fail.
	 */
	@Test
	public void tryUpdatingToExistingName() {
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Double room");
		
		assertFalse(administrator.updateRoomType("Double room", "Triple room", 2000, 3, extras));
		
	}
	
	/**
	 * Tries to update a room type with a non positive amount of beds, should fail.
	 */
	@Test
	public void tryUpdatingTooFewBeds() {
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Double room");
		
		assertFalse(administrator.updateRoomType("Double room", "Double room", 2000, 0, extras));
		
	}
	
	/**
	 * updates the room type on its every feature
	 */
	@Test
	public void shouldUpdateRoomType() {
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Double room");
		assertTrue(roomType.getPrice() == 1000);
		
		extras.clear();
		extra = EntitiesFactory.eINSTANCE.createExtra();
		extra.setDescription("Another extra");
		extra.setPrice(200);
		extras.add(extra);
		
		administrator.updateRoomType("Double room", "An updated double room", 2000, 3, extras);
		
		assertTrue(roomType.getName().contentEquals("Double room") && roomType.getDescription().contentEquals("An updated double room")
				&& roomType.getPrice() == 2000 && roomType.getNumberOfBeds() == 3 && roomType.getExtras().equals(extras));
	}

}




