package allTest;

import org.eclipse.emf.common.util.BasicEList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import Hotel.Entities.Extra;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.impl.AdministratorImpl;

public class AddRoomTypeTest {
	Administrator administrator = new AdministratorImpl();
	
	@Before
	public void setUp() throws Exception {
		administrator.startUpHotelSystem(0);
	}
	
	@Test
	public void testNormal() {
		administrator.addRoomType("TestType", 10, 2, new BasicEList<Extra>(), "TestType");
	
		int count = 0;
		for(RoomType roomType : RoomTypeInterfaceImpl.basicGetInstance().getRoomTypes()) {
			if(roomType.getName() == "TestType") {
				count++;
			}
		}
		
		Assert.assertTrue(RoomTypeInterfaceImpl.basicGetInstance().getRoomTypes().size() == 2);
		Assert.assertTrue(count == 1);
	}
	
	@Test
	public void testAddSame() {
		administrator.addRoomType("TestType", 10, 2, new BasicEList<Extra>(), "TestType");
		administrator.addRoomType("TestType", 100, 2, new BasicEList<Extra>(), "TestType");
		
		Assert.assertTrue(RoomTypeInterfaceImpl.basicGetInstance().getRoomTypes().size() == 2);
		Assert.assertTrue(RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("TestType").getPrice() == 10.0);
	}

}
