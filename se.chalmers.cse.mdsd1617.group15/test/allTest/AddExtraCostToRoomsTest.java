package allTest;

import static org.junit.Assert.fail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Entities.Extra;
import Hotel.Entities.impl.EntitiesFactoryImpl;
import Hotel.Room.Room;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.impl.AdministratorImpl;
import Hotel.User.Receptionist.impl.ReceptionistImpl;

public class AddExtraCostToRoomsTest {
	ReceptionistImpl receptionist = new ReceptionistImpl();
	AdministratorImpl administrator = new AdministratorImpl();
	
	EMap<RoomType, Integer> rooms;
	
	int bookingID = -1;
	int roomID = -1;
	
	@Before
	public void setUp() throws Exception {
		administrator.startUpHotelSystem(5);
		// Add a room with a roomType
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Default Description");
		
		// Put the rooms in a list(for makeBooking)
		rooms = new BasicEMap<RoomType, Integer>();
		rooms.put(roomType, 2);
		
		// Try to parse the dates and makeBooking + checkInBooking
		try {
			// Make booking
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date start = format.parse("20161219");
			Date end = format.parse("20161223");
			bookingID = receptionist.makeBooking("Hampus", "Gunnrup", rooms, start, end);
			// Check in
			receptionist.checkInBooking(bookingID);
			roomID = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID).getRooms().get(0).getRoomID();
		} catch(ParseException e) {	
			fail("Parsing error @ AddExtraCostToRooms test");
		}

	}
	
	/*Test with the default values*/
	@Test
	public void testNormal() {
		Extra extra = EntitiesFactoryImpl.init().createExtra();
		extra.setDescription("TestExtra");
		extra.setPrice(10.0);
		receptionist.addRoomExtra(bookingID, roomID, extra.getDescription(), extra.getPrice());

		Assert.assertTrue(BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID).getRoomExtras().size() == 1);
		Assert.assertTrue(BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID).getRoomExtras().get(roomID).getValue().get(0).getDescription() == "TestExtra");
	}
	
	 //@Test
	// running add extra again does not work. There is a java.lang.ArrayStoreException. This happens when the get method is called on a list. This is a problem with EMF.
	public void testExtraExists() {
		Extra extra = EntitiesFactoryImpl.init().createExtra();
		extra.setDescription("TestExtra");
		extra.setPrice(100.0);
		receptionist.addRoomExtra(bookingID, roomID, extra.getDescription(), extra.getPrice());
		receptionist.addRoomExtra(bookingID, roomID, extra.getDescription(), extra.getPrice());
		
		Assert.assertTrue(BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID).getRoomExtras().get(roomID) != null);
		Assert.assertTrue(BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID).getRoomExtras().get(roomID).getValue().get(0).getDescription() == "TestExtra");
		Assert.assertTrue(BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID).getRoomExtras().get(roomID).getValue().get(0).getPrice() == 10.0);
	}
	
	@Test
	public void testBookingIDToLarge() {
		Extra extra = EntitiesFactoryImpl.init().createExtra();
		extra.setDescription("TestExtra");
		extra.setPrice(10.0);
		receptionist.addRoomExtra(bookingID+1, roomID, extra.getDescription(), extra.getPrice());
		
		Assert.assertTrue(BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID).getRoomExtras().size() == 0);
	}
	
	@Test
	public void testBookingIDToSmall() {
		Extra extra = EntitiesFactoryImpl.init().createExtra();
		extra.setDescription("TestExtra");
		extra.setPrice(10.0);
		receptionist.addRoomExtra(-1, roomID, extra.getDescription(), extra.getPrice());
		
		Assert.assertTrue(BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID).getRoomExtras().size() == 0);
	}
	
	@Test
	public void testWrongBookingID() {
		int bookingID2 = -1;
		// Try to parse the dates and makeBooking + checkInBooking
		try {
			// Make booking with no rooms
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date start = format.parse("20161219");
			Date end = format.parse("20161223");
			bookingID2 = receptionist.makeBooking("Hampus", "Gunnrup", rooms, start, end);
			// Check in
			EList<Room> checkIn = new BasicEList<Room>();
			checkIn.add(RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomID));
			receptionist.checkInBooking(bookingID);
		} catch(ParseException e) {	
			fail("Parsing error @ AddExtraCostToRooms test");
		}
		
		Extra extra = EntitiesFactoryImpl.init().createExtra();
		extra.setDescription("TestExtra");
		extra.setPrice(10.0);
		receptionist.addRoomExtra(bookingID2, roomID, extra.getDescription(), extra.getPrice()); // Try adding extra to a room that does not belong
		
		Assert.assertTrue(BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID).getRoomExtras().size() == 0);
		Assert.assertTrue(BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID2).getRoomExtras().size() == 0);
	}
}