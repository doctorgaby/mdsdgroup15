package allTest;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.junit.Before;
import org.junit.Test;

import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Room.Room;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistFactory;

/**
 * Test class for use case 2.1.12 - Check out and pay room
 * 
 * @author Christoffer
 */
public class CheckOutAndPayRoomTest {

	Administrator administrator;
	Receptionist receptionist;
	String startDate;
	String endDate;
	String firstName;
	String lastName;
	EMap<RoomType,Integer> roomTypeAmounts;
	int bookingId;
	RoomType roomType;
	Booking booking;
	List<Room> rooms;
	String ccNumber;
	String ccv;
	int expiryMonth;
	int expiryYear;
	double balanceBefore = -1;
	double balanceAfter = -1;
	
	@Before
	public void setUp(){
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		receptionist = ReceptionistFactory.eINSTANCE.createReceptionist();
		startDate = "20150101";
		endDate = "20181220";
		firstName = "Sohn";
		lastName = "Toe";
		ccNumber = "15000099";
		ccv = "157";
		expiryMonth = 12;
		expiryYear = 16;
		roomTypeAmounts = new BasicEMap<RoomType,Integer>();
		roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription("Default Description");
		roomTypeAmounts.put(roomType, 5);
		
		//Initiate booking
		bookingId = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		
		//Book rooms
		for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts){
			for(int i = 0; i < entry.getValue(); i++){
				receptionist.addRoomToBooking(roomType.getDescription(), bookingId);
			}
		}
		
		//Confirm booking
		receptionist.confirmBooking(bookingId);
		
		//Check in booking
		receptionist.checkInBooking(bookingId);
		
		//Get the booking object
		booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		
		//Get rooms of booking
		rooms = booking.getRooms();
	}
	
	/**
	 * Makes sure that the total cost is calculated correctly (initiateRoomCheckout).
	 */
	@Test
	public void initiateRoomCheckoutCost() {
		double totalSum = 0;
		for(Room room : rooms){			//We will have 5 rooms, each of cost 1000
			totalSum += receptionist.initiateRoomCheckout(room.getRoomID(), bookingId);
		}
		assertTrue(totalSum==5000.0);	//The total cost should be 5*1000 = 5000
	}

	/**
	 * Makes sure that all rooms are set to unpaid (initiateRoomCheckout).
	 */
	@Test
	public void initiateRoomCheckoutRoomsUnpaid() {
		for(Room room : rooms){
			receptionist.initiateRoomCheckout(room.getRoomID(), bookingId);
			assertFalse(!room.isUnpaid());
		}
	}
	
	/**
	 * Checks that the credit card balance gets updated successfully upon payment.
	 */
	@Test
	public void payRoomDuringCheckoutCorrectBalance(){
		try {
			se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires bankAdmin = se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires.instance();
			se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires bank = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
			
			//Reset bank card
			bankAdmin.removeCreditCard(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
			bankAdmin.addCreditCard(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
			
			//Add 10000 to the credit card and get the balance
			if(bank.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName)){
				bankAdmin.makeDeposit(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, 10000);
				balanceBefore = bankAdmin.getBalance(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
				System.out.println("Balance before: " + balanceBefore);
			} else {
				fail("Invalid card!");
			}
			
			//Pay each room of the booking (of total cost 5000)
			for(Room room : rooms){
				if(bank.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName)){
					assertFalse(!receptionist.payRoomDuringCheckout(room.getRoomID(), ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName));	//5 rooms, each of cost 1000 (Make sure all payments succeed)
				} else {
					fail("Invalid card!");
				}
			}
			
			//Get the balance after the payments
			balanceAfter = bankAdmin.getBalance(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
			System.out.println("Balance after: " + balanceAfter);
			
			//Make sure that the difference in balance is 5000
			assertTrue(balanceBefore-balanceAfter==5000);
			} catch (SOAPException e) {
				fail("Error occurred while communicating with the bank administration!");
			}
	}
	

}
