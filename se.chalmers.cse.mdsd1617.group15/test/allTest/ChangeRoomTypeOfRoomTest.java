package allTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Hotel.Room.Room;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorFactory;

/**
 * 
 * Test class for UC 2.2.5 "Change room type"
 *
 */
public class ChangeRoomTypeOfRoomTest {

	Administrator administrator;
	int roomId;
	String roomTypeDescription;
	String newRoomTypeDescription;
	
	
	@Before
	public void setUp() throws Exception {
		
		roomTypeDescription = "Old";
		newRoomTypeDescription = "New";
		roomId = 0;
		administrator = AdministratorFactory.eINSTANCE.createAdministrator();
		administrator.startUpHotelSystem(10);
		administrator.addRoomType(roomTypeDescription, 600, 1, null, roomTypeDescription);
		administrator.addRoomType(newRoomTypeDescription, 600, 1, null, newRoomTypeDescription);
		RoomInterfaceImpl.basicGetInstance().clearComponent();
		administrator.addRoom(roomId, roomTypeDescription);
	}
	
	/**
	 * Trying to change room type to the same as the current one. 
	 * Should return false, but room should keep its old room type.
	 */
	@Test
	public void testChangeToCurrentRoomType() {
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomId);
		
		assertTrue(room.getRoomType().getDescription().equals(roomTypeDescription));
		
		assertFalse(administrator.changeRoomType(roomId, roomTypeDescription));
		
		assertTrue(room.getRoomType().getDescription().equals(roomTypeDescription));
		
	}
	
	/**
	 * Trying to change room type to a non existent type. 
	 * Should return false, but room should keep its old room type.
	 */
	@Test
	public void testChangeToNonExistentRoomType() {
		
		String nonExistentRoomType = "Do not exist";
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomId);
		
		assertTrue(room.getRoomType().getDescription().equals(roomTypeDescription));
		
		assertFalse(administrator.changeRoomType(roomId, nonExistentRoomType));
		
		assertTrue(room.getRoomType().getDescription().equals(roomTypeDescription));
		
	}
	
	/**
	 * Trying to change room type on a non existent room (roomId = 2)
	 * Should return false.
	 */
	@Test
	public void testChangeRoomTypeOfNonExistentRoom() {
		
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(2);
		
		assertFalse(administrator.changeRoomType(2, newRoomTypeDescription));
		
	}
	
	/**
	 * Should change room type of a room to a new one. 
	 */
	@Test
	public void roomTypeShouldChange() {
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomId);
		
		assertTrue(room.getRoomType().getDescription().equals(roomTypeDescription));
		
		assertTrue(administrator.changeRoomType(roomId, newRoomTypeDescription));
		assertTrue(room.getRoomType().getDescription().equals(newRoomTypeDescription));
		
	}

}
