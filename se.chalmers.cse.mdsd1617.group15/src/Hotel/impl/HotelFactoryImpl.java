/**
 */
package Hotel.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import Hotel.FreeRoomTypesDTO;
import Hotel.HotelFactory;
import Hotel.HotelPackage;
import Hotel.IHotelCustomerProvides;
import Hotel.IHotelStartupProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HotelFactoryImpl extends EFactoryImpl implements HotelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static HotelFactory init() {
		try {
			HotelFactory theHotelFactory = (HotelFactory)EPackage.Registry.INSTANCE.getEFactory(HotelPackage.eNS_URI);
			if (theHotelFactory != null) {
				return theHotelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new HotelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case HotelPackage.IHOTEL_STARTUP_PROVIDES: return createIHotelStartupProvides();
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES: return createIHotelCustomerProvides();
			case HotelPackage.FREE_ROOM_TYPES_DTO: return createFreeRoomTypesDTO();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelStartupProvides createIHotelStartupProvides() {
		IHotelStartupProvidesImpl iHotelStartupProvides = new IHotelStartupProvidesImpl();
		return iHotelStartupProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelCustomerProvides createIHotelCustomerProvides() {
		IHotelCustomerProvidesImpl iHotelCustomerProvides = new IHotelCustomerProvidesImpl();
		return iHotelCustomerProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreeRoomTypesDTO createFreeRoomTypesDTO() {
		FreeRoomTypesDTOImpl freeRoomTypesDTO = new FreeRoomTypesDTOImpl();
		return freeRoomTypesDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelPackage getHotelPackage() {
		return (HotelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static HotelPackage getPackage() {
		return HotelPackage.eINSTANCE;
	}

} //HotelFactoryImpl
