/**
 */
package Hotel.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.FreeRoomTypesDTO;
import Hotel.HotelPackage;
import Hotel.IHotelCustomerProvides;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Room.Room;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IHotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IHotelCustomerProvidesImpl extends MinimalEObjectImpl.Container implements IHotelCustomerProvides {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IHotelCustomerProvidesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HotelPackage.Literals.IHOTEL_CUSTOMER_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		EList<FreeRoomTypesDTO> freeRoomTypesDtoList = new BasicEList<FreeRoomTypesDTO>();

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date start = format.parse(startDate);
			Date end = format.parse(endDate);
			EMap<RoomType, EList<Room>> freeRooms = RoomInterfaceImpl.basicGetInstance().getRoomLister().searchFreeRooms(start, end, numBeds);
			for(Map.Entry<RoomType, EList<Room>> entry : freeRooms){
				RoomType roomType = entry.getKey();
				FreeRoomTypesDTO freeRoomTypesDto = new FreeRoomTypesDTOImpl();
				freeRoomTypesDto.setRoomTypeDescription(roomType.getDescription());
				freeRoomTypesDto.setNumBeds(roomType.getNumberOfBeds());
				freeRoomTypesDto.setPricePerNight(roomType.getPrice());
				freeRoomTypesDto.setNumFreeRooms(entry.getValue().size());
				freeRoomTypesDtoList.add(freeRoomTypesDto);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return freeRoomTypesDtoList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().initiateBooking(firstName, startDate, endDate, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().addRoomToBooking(roomTypeDescription, bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().initiateCheckout(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().payDuringCheckout(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingId) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().initiateRoomCheckout(roomNumber, bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().payRoomDuringCheckout(roomNumber, ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingId) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().checkInRoom(roomTypeDescription, bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case HotelPackage.IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //IHotelCustomerProvidesImpl
