/**
 */
package Hotel.Room;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.Room.RoomPackage#getRoomModifier()
 * @model
 * @generated
 */
public interface RoomModifier extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	int addRoom(int roomNumber, String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	boolean blockRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	boolean removeRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	boolean unblockRoom(int roomID);

} // RoomModifier
