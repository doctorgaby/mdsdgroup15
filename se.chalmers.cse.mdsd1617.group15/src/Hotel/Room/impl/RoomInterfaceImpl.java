/**
 */
package Hotel.Room.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import Hotel.Room.IRoomInterface;
import Hotel.Room.Room;
import Hotel.Room.RoomClass;
import Hotel.Room.RoomInterface;
import Hotel.Room.RoomLister;
import Hotel.Room.RoomModifier;
import Hotel.Room.RoomPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Room.impl.RoomInterfaceImpl#getRoomClass <em>Room Class</em>}</li>
 *   <li>{@link Hotel.Room.impl.RoomInterfaceImpl#getRoomLister <em>Room Lister</em>}</li>
 *   <li>{@link Hotel.Room.impl.RoomInterfaceImpl#getRoomModifier <em>Room Modifier</em>}</li>
 *   <li>{@link Hotel.Room.impl.RoomInterfaceImpl#getRooms <em>Rooms</em>}</li>
 *   <li>{@link Hotel.Room.impl.RoomInterfaceImpl#getInstance <em>Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomInterfaceImpl extends MinimalEObjectImpl.Container implements RoomInterface {
	/**
	 * The cached value of the '{@link #getRoomClass() <em>Room Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomClass()
	 * @generated
	 * @ordered
	 */
	protected RoomClass roomClass;

	/**
	 * The cached value of the '{@link #getRoomLister() <em>Room Lister</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomLister()
	 * @generated
	 * @ordered
	 */
	protected RoomLister roomLister;

	/**
	 * The cached value of the '{@link #getRoomModifier() <em>Room Modifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomModifier()
	 * @generated
	 * @ordered
	 */
	protected RoomModifier roomModifier;

	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	/**
	 * The cached value of the '{@link #getInstance() <em>Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstance()
	 * @generated NOT
	 * @ordered
	 */
	private static IRoomInterface instance;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private RoomInterfaceImpl() {
		super();
		roomClass = new RoomClassImpl();
		roomLister = new RoomListerImpl();
		roomModifier = new RoomModifierImpl();
		rooms = new BasicEList<Room>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_INTERFACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomClass getRoomClass() {
		if (roomClass != null && roomClass.eIsProxy()) {
			InternalEObject oldRoomClass = (InternalEObject)roomClass;
			roomClass = (RoomClass)eResolveProxy(oldRoomClass);
			if (roomClass != oldRoomClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomPackage.ROOM_INTERFACE__ROOM_CLASS, oldRoomClass, roomClass));
			}
		}
		return roomClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomClass basicGetRoomClass() {
		return roomClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomClass(RoomClass newRoomClass) {
		RoomClass oldRoomClass = roomClass;
		roomClass = newRoomClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_INTERFACE__ROOM_CLASS, oldRoomClass, roomClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomLister getRoomLister() {
		if (roomLister != null && roomLister.eIsProxy()) {
			InternalEObject oldRoomLister = (InternalEObject)roomLister;
			roomLister = (RoomLister)eResolveProxy(oldRoomLister);
			if (roomLister != oldRoomLister) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomPackage.ROOM_INTERFACE__ROOM_LISTER, oldRoomLister, roomLister));
			}
		}
		return roomLister;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomLister basicGetRoomLister() {
		return roomLister;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomLister(RoomLister newRoomLister) {
		RoomLister oldRoomLister = roomLister;
		roomLister = newRoomLister;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_INTERFACE__ROOM_LISTER, oldRoomLister, roomLister));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomModifier getRoomModifier() {
		if (roomModifier != null && roomModifier.eIsProxy()) {
			InternalEObject oldRoomModifier = (InternalEObject)roomModifier;
			roomModifier = (RoomModifier)eResolveProxy(oldRoomModifier);
			if (roomModifier != oldRoomModifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomPackage.ROOM_INTERFACE__ROOM_MODIFIER, oldRoomModifier, roomModifier));
			}
		}
		return roomModifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomModifier basicGetRoomModifier() {
		return roomModifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomModifier(RoomModifier newRoomModifier) {
		RoomModifier oldRoomModifier = roomModifier;
		roomModifier = newRoomModifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_INTERFACE__ROOM_MODIFIER, oldRoomModifier, roomModifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectResolvingEList<Room>(Room.class, this, RoomPackage.ROOM_INTERFACE__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IRoomInterface getInstance() {
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public static IRoomInterface basicGetInstance() {
		if(instance == null){
			instance = new RoomInterfaceImpl();
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstance(IRoomInterface newInstance) {
		IRoomInterface oldInstance = instance;
		instance = newInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_INTERFACE__INSTANCE, oldInstance, instance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearComponent() {
		rooms.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Room getRoomFromId(int roomId) {
		for(Room room : rooms){
			if(room.getRoomID() == roomId){
				return room;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM_INTERFACE__ROOM_CLASS:
				if (resolve) return getRoomClass();
				return basicGetRoomClass();
			case RoomPackage.ROOM_INTERFACE__ROOM_LISTER:
				if (resolve) return getRoomLister();
				return basicGetRoomLister();
			case RoomPackage.ROOM_INTERFACE__ROOM_MODIFIER:
				if (resolve) return getRoomModifier();
				return basicGetRoomModifier();
			case RoomPackage.ROOM_INTERFACE__ROOMS:
				return getRooms();
			case RoomPackage.ROOM_INTERFACE__INSTANCE:
				if (resolve) return getInstance();
				return basicGetInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM_INTERFACE__ROOM_CLASS:
				setRoomClass((RoomClass)newValue);
				return;
			case RoomPackage.ROOM_INTERFACE__ROOM_LISTER:
				setRoomLister((RoomLister)newValue);
				return;
			case RoomPackage.ROOM_INTERFACE__ROOM_MODIFIER:
				setRoomModifier((RoomModifier)newValue);
				return;
			case RoomPackage.ROOM_INTERFACE__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
			case RoomPackage.ROOM_INTERFACE__INSTANCE:
				setInstance((IRoomInterface)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_INTERFACE__ROOM_CLASS:
				setRoomClass((RoomClass)null);
				return;
			case RoomPackage.ROOM_INTERFACE__ROOM_LISTER:
				setRoomLister((RoomLister)null);
				return;
			case RoomPackage.ROOM_INTERFACE__ROOM_MODIFIER:
				setRoomModifier((RoomModifier)null);
				return;
			case RoomPackage.ROOM_INTERFACE__ROOMS:
				getRooms().clear();
				return;
			case RoomPackage.ROOM_INTERFACE__INSTANCE:
				setInstance((IRoomInterface)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_INTERFACE__ROOM_CLASS:
				return roomClass != null;
			case RoomPackage.ROOM_INTERFACE__ROOM_LISTER:
				return roomLister != null;
			case RoomPackage.ROOM_INTERFACE__ROOM_MODIFIER:
				return roomModifier != null;
			case RoomPackage.ROOM_INTERFACE__ROOMS:
				return rooms != null && !rooms.isEmpty();
			case RoomPackage.ROOM_INTERFACE__INSTANCE:
				return instance != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_INTERFACE___CLEAR_COMPONENT:
				clearComponent();
				return null;
			case RoomPackage.ROOM_INTERFACE___GET_ROOM_FROM_ID__INT:
				return getRoomFromId((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomInterfaceImpl
