/**
 */
package Hotel.Room.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Room.Room;
import Hotel.Room.RoomClass;
import Hotel.Room.RoomPackage;
import Hotel.Room.RoomStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RoomClassImpl extends MinimalEObjectImpl.Container implements RoomClass {
	private static final Object RoomType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(int bookingID, String roomTypeDescription) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID);

		if(booking == null){
			return -1;
		}
	
		EList<Room> rooms = booking.getRooms();
		for(Room room : rooms){
			if(room.getRoomType().getDescription().equals(roomTypeDescription) && room.getRoomStatus().equals(RoomStatus.BOOKED)){
				room.setRoomStatus(RoomStatus.OCCUPIED);
				return room.getRoomID();
			}
		}
		
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_CLASS___CHECK_IN_ROOM__INT_STRING:
				return checkInRoom((Integer)arguments.get(0), (String)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomClassImpl
