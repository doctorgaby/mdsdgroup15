/**
 */
package Hotel.Room.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Entities.PaymentStatus;
import Hotel.Room.Room;
import Hotel.Room.RoomModifier;
import Hotel.Room.RoomPackage;
import Hotel.Room.RoomStatus;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Modifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RoomModifierImpl extends MinimalEObjectImpl.Container implements RoomModifier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomModifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_MODIFIER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int addRoom(int roomNumber, String roomTypeDescription) {
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription(roomTypeDescription);
		if(roomType != null){
			EList<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
			for (Room room : rooms){
				if (room.getRoomID() == roomNumber){
					return -1;
				}
			}
			Room room = new RoomImpl();
			room.setRoomID(roomNumber);
			room.setRoomStatus(RoomStatus.FREE);
			room.setPaymentStatus(PaymentStatus.UNPAID);
			room.setRoomType(roomType);
			rooms.add(room);
			
			return room.getRoomID();
		}
		
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean blockRoom(int roomID) {
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomID);
		if(room != null){
			room.setRoomStatus(RoomStatus.BLOCKED);
			return true;
		} else {
			System.out.println("A room with ID " + roomID + " does not exist!");
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomID) {
		EList<Room> rooms = RoomInterfaceImpl.basicGetInstance().getRooms();
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomID);
		if(room != null){
			EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
			for(Booking booking : bookings){
				for(Room r : booking.getRooms()){
					if(r.equals(room) && booking.isCheckedIn()){
						return false;	//Return false if a booking is checked in to the room
					}
				}
			}
			rooms.remove(room);
			return true;
		} else {
			System.out.println("A room with ID " + roomID + " does not exist!");
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean unblockRoom(int roomID) {
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomID);
		if(room != null){
			if (!room.isBlocked()){
				return false;
			}
			room.setRoomStatus(RoomStatus.FREE);
			return true;
		} else {
			System.out.println("A room with ID " + roomID + " does not exist!");
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_MODIFIER___ADD_ROOM__INT_STRING:
				return addRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case RoomPackage.ROOM_MODIFIER___BLOCK_ROOM__INT:
				return blockRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_MODIFIER___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_MODIFIER___UNBLOCK_ROOM__INT:
				return unblockRoom((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomModifierImpl
