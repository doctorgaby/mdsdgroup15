/**
 */
package Hotel.Room;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Room.IRoomInterface#getRoomClass <em>Room Class</em>}</li>
 *   <li>{@link Hotel.Room.IRoomInterface#getRoomLister <em>Room Lister</em>}</li>
 *   <li>{@link Hotel.Room.IRoomInterface#getRoomModifier <em>Room Modifier</em>}</li>
 *   <li>{@link Hotel.Room.IRoomInterface#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @see Hotel.Room.RoomPackage#getIRoomInterface()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomInterface extends EObject {
	/**
	 * Returns the value of the '<em><b>Room Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Class</em>' reference.
	 * @see #setRoomClass(RoomClass)
	 * @see Hotel.Room.RoomPackage#getIRoomInterface_RoomClass()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomClass getRoomClass();

	/**
	 * Sets the value of the '{@link Hotel.Room.IRoomInterface#getRoomClass <em>Room Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Class</em>' reference.
	 * @see #getRoomClass()
	 * @generated
	 */
	void setRoomClass(RoomClass value);

	/**
	 * Returns the value of the '<em><b>Room Lister</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Lister</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Lister</em>' reference.
	 * @see #setRoomLister(RoomLister)
	 * @see Hotel.Room.RoomPackage#getIRoomInterface_RoomLister()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomLister getRoomLister();

	/**
	 * Sets the value of the '{@link Hotel.Room.IRoomInterface#getRoomLister <em>Room Lister</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Lister</em>' reference.
	 * @see #getRoomLister()
	 * @generated
	 */
	void setRoomLister(RoomLister value);

	/**
	 * Returns the value of the '<em><b>Room Modifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Modifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Modifier</em>' reference.
	 * @see #setRoomModifier(RoomModifier)
	 * @see Hotel.Room.RoomPackage#getIRoomInterface_RoomModifier()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomModifier getRoomModifier();

	/**
	 * Sets the value of the '{@link Hotel.Room.IRoomInterface#getRoomModifier <em>Room Modifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Modifier</em>' reference.
	 * @see #getRoomModifier()
	 * @generated
	 */
	void setRoomModifier(RoomModifier value);

	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' reference list.
	 * The list contents are of type {@link Hotel.Room.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' reference list.
	 * @see Hotel.Room.RoomPackage#getIRoomInterface_Rooms()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Room> getRooms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearComponent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIdRequired="true" roomIdOrdered="false"
	 * @generated
	 */
	Room getRoomFromId(int roomId);

} // IRoomInterface
