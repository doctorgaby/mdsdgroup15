/**
 */
package Hotel.User.Receptionist.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Booking.Booking;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Entities.Extra;
import Hotel.Entities.impl.EntitiesFactoryImpl;
import Hotel.Entities.impl.RoomToExtraMapEntryImpl;
import Hotel.Room.Room;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.User.Receptionist.Receptionist;
import Hotel.User.Receptionist.ReceptionistPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Receptionist</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ReceptionistImpl extends MinimalEObjectImpl.Container implements Receptionist {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ReceptionistImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ReceptionistPackage.Literals.RECEPTIONIST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listCheckOuts(Date fromDay, Date toDay) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().listCheckOuts(fromDay, toDay);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listBookings() {
		return BookingInterfaceImpl.basicGetInstance().getBooker().listBookings();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listCheckIns(Date fromDay, Date toDay) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().listCheckIns(fromDay, toDay);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingID) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().cancelBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInBooking(int bookingID) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().checkInBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().payDuringCheckout(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<RoomType, EList<Room>> searchFreeRooms(Date fromDay, Date toDay, int minNumberOfBeds) {
		return RoomInterfaceImpl.basicGetInstance().getRoomLister().searchFreeRooms(fromDay, toDay, minNumberOfBeds);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Room> listOccupiedRooms(Date day) {
		return RoomInterfaceImpl.basicGetInstance().getRoomLister().listOccupiedRooms(day);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(int bookingID, String roomTypeDescription) {
		return RoomInterfaceImpl.basicGetInstance().getRoomClass().checkInRoom(bookingID, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addRoomExtra(int bookingID, int roomID, String extraDescription, double price) {
		// Get the booking
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID);
		
		// If the booking exists
		if(null != booking) {
			// Get the room
			Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomID);
			boolean roomExists = false;
			
			for(Room roomInBooking : booking.getRooms()) {
				if(roomInBooking.getRoomID() == roomID) {
					roomExists = true;
				}
			}

			// If the room exist in the booking
			if(roomExists) {
				// Create the extra
				Extra extra = EntitiesFactoryImpl.init().createExtra();
				extra.setDescription(extraDescription);
				extra.setPrice(price);
			
				// Check if an entry with that room exists
				if(null != booking.getRoomExtras() && null != booking.getRoomExtras().get(room)) {
					booking.getRoomExtras().get(room).add(extra);
				} else {
					// Put the extra in a list
					EList<Extra> extras = new BasicEList<Extra>();
					extras.add(extra);
					
					// Add the extra to a new entry
					RoomToExtraMapEntryImpl entry = new RoomToExtraMapEntryImpl();
					entry.setKey(room);
					entry.setValue(extras);
					
					// Add the new entry to the booking extras map
					booking.getRoomExtras().add(entry);
				}
			} else {
				System.out.println("Room does not exist in booking @Receptionist.addRoomExtra()");
			}
		} else {
			System.out.println("Booking does not exist @Receptionist.addRoomExtra()");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean editBooking(int bookingID, EMap<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().editBooking(bookingID, roomTypeToInteger, fromDay, toDay);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int makeBooking(String firstName, String lastName, EMap<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().makeBooking(firstName, lastName, roomTypeToInteger, fromDay, toDay);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().initiateBooking(firstName, startDate, endDate, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().addRoomToBooking(roomTypeDescription, bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingId) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().initiateRoomCheckout(roomNumber, bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().payRoomDuringCheckout(roomNumber, ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().initiateCheckout(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ReceptionistPackage.RECEPTIONIST___LIST_CHECK_OUTS__DATE_DATE:
				return listCheckOuts((Date)arguments.get(0), (Date)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___LIST_BOOKINGS:
				return listBookings();
			case ReceptionistPackage.RECEPTIONIST___LIST_CHECK_INS__DATE_DATE:
				return listCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case ReceptionistPackage.RECEPTIONIST___CHECK_IN_BOOKING__INT:
				return checkInBooking((Integer)arguments.get(0));
			case ReceptionistPackage.RECEPTIONIST___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case ReceptionistPackage.RECEPTIONIST___SEARCH_FREE_ROOMS__DATE_DATE_INT:
				return searchFreeRooms((Date)arguments.get(0), (Date)arguments.get(1), (Integer)arguments.get(2));
			case ReceptionistPackage.RECEPTIONIST___LIST_OCCUPIED_ROOMS__DATE:
				return listOccupiedRooms((Date)arguments.get(0));
			case ReceptionistPackage.RECEPTIONIST___CHECK_IN_ROOM__INT_STRING:
				return checkInRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___ADD_ROOM_EXTRA__INT_INT_STRING_DOUBLE:
				addRoomExtra((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
				return null;
			case ReceptionistPackage.RECEPTIONIST___EDIT_BOOKING__INT_EMAP_DATE_DATE:
				return editBooking((Integer)arguments.get(0), (EMap<RoomType, Integer>)arguments.get(1), (Date)arguments.get(2), (Date)arguments.get(3));
			case ReceptionistPackage.RECEPTIONIST___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE:
				return makeBooking((String)arguments.get(0), (String)arguments.get(1), (EMap<RoomType, Integer>)arguments.get(2), (Date)arguments.get(3), (Date)arguments.get(4));
			case ReceptionistPackage.RECEPTIONIST___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case ReceptionistPackage.RECEPTIONIST___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case ReceptionistPackage.RECEPTIONIST___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case ReceptionistPackage.RECEPTIONIST___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ReceptionistImpl
