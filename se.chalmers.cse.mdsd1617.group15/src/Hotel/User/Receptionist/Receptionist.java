/**
 */
package Hotel.User.Receptionist;

import java.util.Date;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import Hotel.Booking.Booking;
import Hotel.Room.Room;
import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Receptionist</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.User.Receptionist.ReceptionistPackage#getReceptionist()
 * @model
 * @generated
 */
public interface Receptionist extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	EList<Booking> listCheckOuts(Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false"
	 * @generated
	 */
	EList<Booking> listBookings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	EList<Booking> listCheckIns(Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	boolean cancelBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	boolean checkInBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" ccNumberRequired="true" ccNumberOrdered="false" ccvRequired="true" ccvOrdered="false" expiryMonthRequired="true" expiryMonthOrdered="false" expiryYearRequired="true" expiryYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="Hotel.Entities.RoomTypeToRoomMapEntry<Hotel.RoomType.RoomType, Hotel.Room.Room>" ordered="false" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false" minNumberOfBedsRequired="true" minNumberOfBedsOrdered="false"
	 * @generated
	 */
	EMap<RoomType, EList<Room>> searchFreeRooms(Date fromDay, Date toDay, int minNumberOfBeds);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dayRequired="true" dayOrdered="false"
	 * @generated
	 */
	EList<Room> listOccupiedRooms(Date day);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	int checkInRoom(int bookingID, String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false" roomIDRequired="true" roomIDOrdered="false" extraDescriptionRequired="true" extraDescriptionOrdered="false" priceRequired="true" priceOrdered="false"
	 * @generated
	 */
	void addRoomExtra(int bookingID, int roomID, String extraDescription, double price);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false" roomTypeToIntegerMapType="Hotel.Entities.RoomTypeToIntegerMapEntry<Hotel.RoomType.RoomType, org.eclipse.emf.ecore.EIntegerObject>" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	boolean editBooking(int bookingID, EMap<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false" roomTypeToIntegerMapType="Hotel.Entities.RoomTypeToIntegerMapEntry<Hotel.RoomType.RoomType, org.eclipse.emf.ecore.EIntegerObject>" fromDayRequired="true" fromDayOrdered="false" toDayRequired="true" toDayOrdered="false"
	 * @generated
	 */
	int makeBooking(String firstName, String lastName, EMap<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" firstNameRequired="true" firstNameOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	int initiateBooking(String firstName, String startDate, String endDate, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	boolean addRoomToBooking(String roomTypeDescription, int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	boolean confirmBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" bookingIdRequired="true" bookingIdOrdered="false"
	 * @generated
	 */
	double initiateRoomCheckout(int roomNumber, int bookingId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" ccNumberRequired="true" ccNumberOrdered="false" ccvRequired="true" ccvOrdered="false" expiryMonthRequired="true" expiryMonthOrdered="false" expiryYearRequired="true" expiryYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	double initiateCheckout(int bookingID);

} // Receptionist
