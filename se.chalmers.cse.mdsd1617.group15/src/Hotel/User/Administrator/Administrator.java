/**
 */
package Hotel.User.Administrator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import Hotel.Entities.Extra;
import Hotel.RoomType.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Administrator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.User.Administrator.AdministratorPackage#getAdministrator()
 * @model
 * @generated
 */
public interface Administrator extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	boolean removeRoomType(String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	boolean changeRoomType(int roomNumber, String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	boolean removeRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	boolean unblockRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	boolean blockRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false"
	 * @generated
	 */
	int addRoom(int roomNumber, String roomTypeDescription);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeToChangeRequired="true" roomTypeToChangeOrdered="false" newDescriptionRequired="true" newDescriptionOrdered="false" priceRequired="true" priceOrdered="false" nrOfBedsRequired="true" nrOfBedsOrdered="false" extrasMany="true" extrasOrdered="false"
	 * @generated
	 */
	boolean updateRoomType(String roomTypeToChange, String newDescription, double price, int nrOfBeds, EList<Extra> extras);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomsRequired="true" roomsOrdered="false"
	 * @generated
	 */
	boolean startUpHotelSystem(int rooms);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false" priceRequired="true" priceOrdered="false" nrOfBedsRequired="true" nrOfBedsOrdered="false" extrasMany="true" extrasOrdered="false" descriptionRequired="true" descriptionOrdered="false"
	 * @generated
	 */
	RoomType addRoomType(String roomType, double price, int nrOfBeds, EList<Extra> extras, String description);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<RoomType> getRoomTypes();

} // Administrator
