/**
 */
package Hotel.User.Administrator.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.HotelFactory;
import Hotel.IHotelStartupProvides;
import Hotel.Entities.Extra;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.RoomTypeHandler;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;
import Hotel.User.Administrator.Administrator;
import Hotel.User.Administrator.AdministratorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Administrator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AdministratorImpl extends MinimalEObjectImpl.Container implements Administrator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public AdministratorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdministratorPackage.Literals.ADMINISTRATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String roomTypeDescription) {
		return RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeHandler().removeRoomType(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomType(int roomNumber, String roomTypeDescription) {
		return RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeHandler().changeRoomType(roomNumber, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomID) {
		return RoomInterfaceImpl.basicGetInstance().getRoomModifier().removeRoom(roomID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean unblockRoom(int roomID) {
		return RoomInterfaceImpl.basicGetInstance().getRoomModifier().unblockRoom(roomID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean blockRoom(int roomID) {
		return RoomInterfaceImpl.basicGetInstance().getRoomModifier().blockRoom(roomID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int addRoom(int roomNumber, String roomTypeDescription) {
		int roomID = RoomInterfaceImpl.basicGetInstance().getRoomModifier().addRoom(roomNumber, roomTypeDescription);
		return roomID; // -1 if adding fails
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String roomTypeToChange, String newDescription, double price, int nrOfBeds, EList<Extra> extras) {
		RoomTypeHandler roomTypeHandler = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeHandler();
		return roomTypeHandler.updateRoomType(roomTypeToChange, newDescription, price, nrOfBeds, extras);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean startUpHotelSystem(int rooms) {
		IHotelStartupProvides hotel = HotelFactory.eINSTANCE.createIHotelStartupProvides();
		
		try {
			hotel.startup(rooms);
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return 
	 * @generated NOT
	 */
	public RoomType addRoomType(String roomType, double price, int nrOfBeds, EList<Extra> extras, String description) {
		RoomTypeHandler roomTypeHandler = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeHandler();
		roomTypeHandler.addRoomType(roomType, price, nrOfBeds, extras, description);
		return RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription(description);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomType> getRoomTypes() {
		return RoomTypeInterfaceImpl.basicGetInstance().getRoomTypes();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AdministratorPackage.ADMINISTRATOR___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case AdministratorPackage.ADMINISTRATOR___CHANGE_ROOM_TYPE__INT_STRING:
				return changeRoomType((Integer)arguments.get(0), (String)arguments.get(1));
			case AdministratorPackage.ADMINISTRATOR___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
			case AdministratorPackage.ADMINISTRATOR___UNBLOCK_ROOM__INT:
				return unblockRoom((Integer)arguments.get(0));
			case AdministratorPackage.ADMINISTRATOR___BLOCK_ROOM__INT:
				return blockRoom((Integer)arguments.get(0));
			case AdministratorPackage.ADMINISTRATOR___ADD_ROOM__INT_STRING:
				return addRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case AdministratorPackage.ADMINISTRATOR___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_ELIST:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2), (Integer)arguments.get(3), (EList<Extra>)arguments.get(4));
			case AdministratorPackage.ADMINISTRATOR___START_UP_HOTEL_SYSTEM__INT:
				return startUpHotelSystem((Integer)arguments.get(0));
			case AdministratorPackage.ADMINISTRATOR___ADD_ROOM_TYPE__STRING_DOUBLE_INT_ELIST_STRING:
				return addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (EList<Extra>)arguments.get(3), (String)arguments.get(4));
			case AdministratorPackage.ADMINISTRATOR___GET_ROOM_TYPES:
				return getRoomTypes();
		}
		return super.eInvoke(operationID, arguments);
	}

} //AdministratorImpl
