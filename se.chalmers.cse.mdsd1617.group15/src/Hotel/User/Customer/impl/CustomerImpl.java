/**
 */
package Hotel.User.Customer.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Booking.Booker;
import Hotel.Booking.impl.BookingInterfaceImpl;
import Hotel.Entities.CreditCard;
import Hotel.Room.Room;
import Hotel.Room.RoomLister;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.User.Customer.Customer;
import Hotel.User.Customer.CustomerPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Customer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CustomerImpl extends MinimalEObjectImpl.Container implements Customer {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CustomerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CustomerPackage.Literals.CUSTOMER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutBooking(int bookingId, CreditCard creditCard) {
		Booker booker = BookingInterfaceImpl.basicGetInstance().getBooker();
		return booker.checkOutBooking(bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(int bookingId, String roomTypeDescription) {
		return RoomInterfaceImpl.basicGetInstance().getRoomClass().checkInRoom(bookingId, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int makeBooking(String firstName, String lastName, EMap<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay) {
		Booker booker = BookingInterfaceImpl.basicGetInstance().getBooker();
		return booker.makeBooking(firstName, lastName, roomTypeToInteger, fromDay, toDay);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<RoomType, EList<Room>> searchFreeRooms(Date fromDay, Date toDay, int minNumberOfBeds) {
		RoomLister roomLister = RoomInterfaceImpl.basicGetInstance().getRoomLister();
		return roomLister.searchFreeRooms(fromDay, toDay, minNumberOfBeds);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutBooking(int bookingId) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().checkOutBooking(bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().initiateBooking(firstName, startDate, endDate, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().addRoomToBooking(roomTypeDescription, bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingId) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().initiateRoomCheckout(roomNumber, bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return BookingInterfaceImpl.basicGetInstance().getBooker().payRoomDuringCheckout(roomNumber, ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case CustomerPackage.CUSTOMER___CHECK_OUT_BOOKING__INT_CREDITCARD:
				return checkOutBooking((Integer)arguments.get(0), (CreditCard)arguments.get(1));
			case CustomerPackage.CUSTOMER___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE:
				return makeBooking((String)arguments.get(0), (String)arguments.get(1), (EMap<RoomType, Integer>)arguments.get(2), (Date)arguments.get(3), (Date)arguments.get(4));
			case CustomerPackage.CUSTOMER___CHECK_IN_ROOM__INT_STRING:
				return checkInRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case CustomerPackage.CUSTOMER___SEARCH_FREE_ROOMS__DATE_DATE_INT:
				return searchFreeRooms((Date)arguments.get(0), (Date)arguments.get(1), (Integer)arguments.get(2));
			case CustomerPackage.CUSTOMER___CHECK_OUT_BOOKING__INT:
				return checkOutBooking((Integer)arguments.get(0));
			case CustomerPackage.CUSTOMER___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case CustomerPackage.CUSTOMER___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case CustomerPackage.CUSTOMER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case CustomerPackage.CUSTOMER___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case CustomerPackage.CUSTOMER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
		}
		return super.eInvoke(operationID, arguments);
	}

} //CustomerImpl
