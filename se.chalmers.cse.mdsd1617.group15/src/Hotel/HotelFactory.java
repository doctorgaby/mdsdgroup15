/**
 */
package Hotel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Hotel.HotelPackage
 * @generated
 */
public interface HotelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HotelFactory eINSTANCE = Hotel.impl.HotelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>IHotel Startup Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IHotel Startup Provides</em>'.
	 * @generated
	 */
	IHotelStartupProvides createIHotelStartupProvides();

	/**
	 * Returns a new object of class '<em>IHotel Customer Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IHotel Customer Provides</em>'.
	 * @generated
	 */
	IHotelCustomerProvides createIHotelCustomerProvides();

	/**
	 * Returns a new object of class '<em>Free Room Types DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Free Room Types DTO</em>'.
	 * @generated
	 */
	FreeRoomTypesDTO createFreeRoomTypesDTO();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	HotelPackage getHotelPackage();

} //HotelFactory
