/**
 */
package Hotel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IHotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.HotelPackage#getIHotelCustomerProvides()
 * @model
 * @generated
 */
public interface IHotelCustomerProvides extends IHotelCustomerProvidesInterface {
} // IHotelCustomerProvides
