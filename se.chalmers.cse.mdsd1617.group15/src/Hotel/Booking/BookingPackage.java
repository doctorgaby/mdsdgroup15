/**
 */
package Hotel.Booking;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Hotel.Booking.BookingFactory
 * @model kind="package"
 * @generated
 */
public interface BookingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Booking";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///Hotel/Booking.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Hotel.Booking";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BookingPackage eINSTANCE = Hotel.Booking.impl.BookingPackageImpl.init();

	/**
	 * The meta object id for the '{@link Hotel.Booking.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Booking.impl.BookingImpl
	 * @see Hotel.Booking.impl.BookingPackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 0;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__BOOKING_ID = 0;

	/**
	 * The feature id for the '<em><b>Customer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__CUSTOMER = 1;

	/**
	 * The feature id for the '<em><b>Booking Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__BOOKING_STATUS = 2;

	/**
	 * The feature id for the '<em><b>Extra Costs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__EXTRA_COSTS = 3;

	/**
	 * The feature id for the '<em><b>Room Type To Integer</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ROOM_TYPE_TO_INTEGER = 4;

	/**
	 * The feature id for the '<em><b>From Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__FROM_DAY = 5;

	/**
	 * The feature id for the '<em><b>To Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__TO_DAY = 6;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ROOMS = 7;

	/**
	 * The feature id for the '<em><b>Room Extras</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ROOM_EXTRAS = 8;

	/**
	 * The feature id for the '<em><b>Payment Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__PAYMENT_STATUS = 9;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = 10;

	/**
	 * The operation id for the '<em>Is Checked In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___IS_CHECKED_IN = 0;

	/**
	 * The operation id for the '<em>Is Checked Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___IS_CHECKED_OUT = 1;

	/**
	 * The operation id for the '<em>Is Cancelled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___IS_CANCELLED = 2;

	/**
	 * The operation id for the '<em>Is Confirmed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___IS_CONFIRMED = 3;

	/**
	 * The operation id for the '<em>Is Paid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___IS_PAID = 4;

	/**
	 * The operation id for the '<em>Is Unpaid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___IS_UNPAID = 5;

	/**
	 * The operation id for the '<em>Is Unconfirmed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___IS_UNCONFIRMED = 6;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = 7;

	/**
	 * The meta object id for the '{@link Hotel.Booking.impl.BookerImpl <em>Booker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Booking.impl.BookerImpl
	 * @see Hotel.Booking.impl.BookingPackageImpl#getBooker()
	 * @generated
	 */
	int BOOKER = 1;

	/**
	 * The feature id for the '<em><b>Current Checkout</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER__CURRENT_CHECKOUT = 0;

	/**
	 * The number of structural features of the '<em>Booker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___LIST_BOOKINGS = 0;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___EDIT_BOOKING__INT_EMAP_DATE_DATE = 1;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___CANCEL_BOOKING__INT = 2;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___CHECK_IN_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___CHECK_OUT_BOOKING__INT = 4;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___LIST_CHECK_INS__DATE_DATE = 5;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___LIST_CHECK_OUTS__DATE_DATE = 6;

	/**
	 * The operation id for the '<em>Make Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE = 7;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___CHECK_IN_ROOM__STRING_INT = 8;

	/**
	 * The operation id for the '<em>Pay Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___PAY_ROOM__INT_CREDITCARD = 9;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 10;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___ADD_ROOM_TO_BOOKING__STRING_INT = 11;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___CONFIRM_BOOKING__INT = 12;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___INITIATE_CHECKOUT__INT = 13;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 14;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___INITIATE_ROOM_CHECKOUT__INT_INT = 15;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 16;

	/**
	 * The operation id for the '<em>Get Booked Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___GET_BOOKED_ROOMS__INT = 17;

	/**
	 * The operation id for the '<em>Get Booking Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___GET_BOOKING_COST__INT = 18;

	/**
	 * The operation id for the '<em>Get Room Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER___GET_ROOM_COST__INT_INT = 19;

	/**
	 * The number of operations of the '<em>Booker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKER_OPERATION_COUNT = 20;

	/**
	 * The meta object id for the '{@link Hotel.Booking.IBookingInterface <em>IBooking Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Booking.IBookingInterface
	 * @see Hotel.Booking.impl.BookingPackageImpl#getIBookingInterface()
	 * @generated
	 */
	int IBOOKING_INTERFACE = 2;

	/**
	 * The feature id for the '<em><b>Bookings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_INTERFACE__BOOKINGS = 0;

	/**
	 * The feature id for the '<em><b>Booker</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_INTERFACE__BOOKER = 1;

	/**
	 * The number of structural features of the '<em>IBooking Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_INTERFACE_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Clear Component</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_INTERFACE___CLEAR_COMPONENT = 0;

	/**
	 * The operation id for the '<em>Get Booking From Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_INTERFACE___GET_BOOKING_FROM_ID__INT = 1;

	/**
	 * The number of operations of the '<em>IBooking Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_INTERFACE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link Hotel.Booking.impl.BookingInterfaceImpl <em>Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Booking.impl.BookingInterfaceImpl
	 * @see Hotel.Booking.impl.BookingPackageImpl#getBookingInterface()
	 * @generated
	 */
	int BOOKING_INTERFACE = 3;

	/**
	 * The feature id for the '<em><b>Bookings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_INTERFACE__BOOKINGS = IBOOKING_INTERFACE__BOOKINGS;

	/**
	 * The feature id for the '<em><b>Booker</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_INTERFACE__BOOKER = IBOOKING_INTERFACE__BOOKER;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_INTERFACE__INSTANCE = IBOOKING_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_INTERFACE_FEATURE_COUNT = IBOOKING_INTERFACE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Clear Component</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_INTERFACE___CLEAR_COMPONENT = IBOOKING_INTERFACE___CLEAR_COMPONENT;

	/**
	 * The operation id for the '<em>Get Booking From Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_INTERFACE___GET_BOOKING_FROM_ID__INT = IBOOKING_INTERFACE___GET_BOOKING_FROM_ID__INT;

	/**
	 * The number of operations of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_INTERFACE_OPERATION_COUNT = IBOOKING_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Hotel.Booking.BookingStatus <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Hotel.Booking.BookingStatus
	 * @see Hotel.Booking.impl.BookingPackageImpl#getBookingStatus()
	 * @generated
	 */
	int BOOKING_STATUS = 4;


	/**
	 * Returns the meta object for class '{@link Hotel.Booking.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see Hotel.Booking.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Booking.Booking#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see Hotel.Booking.Booking#getBookingId()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_BookingId();

	/**
	 * Returns the meta object for the reference '{@link Hotel.Booking.Booking#getCustomer <em>Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Customer</em>'.
	 * @see Hotel.Booking.Booking#getCustomer()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Customer();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Booking.Booking#getBookingStatus <em>Booking Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Status</em>'.
	 * @see Hotel.Booking.Booking#getBookingStatus()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_BookingStatus();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Booking.Booking#getExtraCosts <em>Extra Costs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extra Costs</em>'.
	 * @see Hotel.Booking.Booking#getExtraCosts()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_ExtraCosts();

	/**
	 * Returns the meta object for the map '{@link Hotel.Booking.Booking#getRoomTypeToInteger <em>Room Type To Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Room Type To Integer</em>'.
	 * @see Hotel.Booking.Booking#getRoomTypeToInteger()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_RoomTypeToInteger();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Booking.Booking#getFromDay <em>From Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From Day</em>'.
	 * @see Hotel.Booking.Booking#getFromDay()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_FromDay();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Booking.Booking#getToDay <em>To Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To Day</em>'.
	 * @see Hotel.Booking.Booking#getToDay()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_ToDay();

	/**
	 * Returns the meta object for the reference list '{@link Hotel.Booking.Booking#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rooms</em>'.
	 * @see Hotel.Booking.Booking#getRooms()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Rooms();

	/**
	 * Returns the meta object for the map '{@link Hotel.Booking.Booking#getRoomExtras <em>Room Extras</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Room Extras</em>'.
	 * @see Hotel.Booking.Booking#getRoomExtras()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_RoomExtras();

	/**
	 * Returns the meta object for the attribute '{@link Hotel.Booking.Booking#getPaymentStatus <em>Payment Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Payment Status</em>'.
	 * @see Hotel.Booking.Booking#getPaymentStatus()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_PaymentStatus();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booking#isCheckedIn() <em>Is Checked In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Checked In</em>' operation.
	 * @see Hotel.Booking.Booking#isCheckedIn()
	 * @generated
	 */
	EOperation getBooking__IsCheckedIn();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booking#isCheckedOut() <em>Is Checked Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Checked Out</em>' operation.
	 * @see Hotel.Booking.Booking#isCheckedOut()
	 * @generated
	 */
	EOperation getBooking__IsCheckedOut();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booking#isCancelled() <em>Is Cancelled</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Cancelled</em>' operation.
	 * @see Hotel.Booking.Booking#isCancelled()
	 * @generated
	 */
	EOperation getBooking__IsCancelled();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booking#isConfirmed() <em>Is Confirmed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Confirmed</em>' operation.
	 * @see Hotel.Booking.Booking#isConfirmed()
	 * @generated
	 */
	EOperation getBooking__IsConfirmed();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booking#isPaid() <em>Is Paid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Paid</em>' operation.
	 * @see Hotel.Booking.Booking#isPaid()
	 * @generated
	 */
	EOperation getBooking__IsPaid();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booking#isUnpaid() <em>Is Unpaid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Unpaid</em>' operation.
	 * @see Hotel.Booking.Booking#isUnpaid()
	 * @generated
	 */
	EOperation getBooking__IsUnpaid();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booking#isUnconfirmed() <em>Is Unconfirmed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Unconfirmed</em>' operation.
	 * @see Hotel.Booking.Booking#isUnconfirmed()
	 * @generated
	 */
	EOperation getBooking__IsUnconfirmed();

	/**
	 * Returns the meta object for class '{@link Hotel.Booking.Booker <em>Booker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booker</em>'.
	 * @see Hotel.Booking.Booker
	 * @generated
	 */
	EClass getBooker();

	/**
	 * Returns the meta object for the reference '{@link Hotel.Booking.Booker#getCurrentCheckout <em>Current Checkout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Checkout</em>'.
	 * @see Hotel.Booking.Booker#getCurrentCheckout()
	 * @see #getBooker()
	 * @generated
	 */
	EReference getBooker_CurrentCheckout();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see Hotel.Booking.Booker#listBookings()
	 * @generated
	 */
	EOperation getBooker__ListBookings();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#editBooking(int, org.eclipse.emf.common.util.EMap, java.util.Date, java.util.Date) <em>Edit Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking</em>' operation.
	 * @see Hotel.Booking.Booker#editBooking(int, org.eclipse.emf.common.util.EMap, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBooker__EditBooking__int_EMap_Date_Date();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see Hotel.Booking.Booker#cancelBooking(int)
	 * @generated
	 */
	EOperation getBooker__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#checkInBooking(int) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see Hotel.Booking.Booker#checkInBooking(int)
	 * @generated
	 */
	EOperation getBooker__CheckInBooking__int();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#checkOutBooking(int) <em>Check Out Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Booking</em>' operation.
	 * @see Hotel.Booking.Booker#checkOutBooking(int)
	 * @generated
	 */
	EOperation getBooker__CheckOutBooking__int();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#listCheckIns(java.util.Date, java.util.Date) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see Hotel.Booking.Booker#listCheckIns(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBooker__ListCheckIns__Date_Date();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#listCheckOuts(java.util.Date, java.util.Date) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see Hotel.Booking.Booker#listCheckOuts(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBooker__ListCheckOuts__Date_Date();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#makeBooking(java.lang.String, java.lang.String, org.eclipse.emf.common.util.EMap, java.util.Date, java.util.Date) <em>Make Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Make Booking</em>' operation.
	 * @see Hotel.Booking.Booker#makeBooking(java.lang.String, java.lang.String, org.eclipse.emf.common.util.EMap, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBooker__MakeBooking__String_String_EMap_Date_Date();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#checkInRoom(java.lang.String, int) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see Hotel.Booking.Booker#checkInRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getBooker__CheckInRoom__String_int();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#payRoom(int, Hotel.Entities.CreditCard) <em>Pay Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room</em>' operation.
	 * @see Hotel.Booking.Booker#payRoom(int, Hotel.Entities.CreditCard)
	 * @generated
	 */
	EOperation getBooker__PayRoom__int_CreditCard();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see Hotel.Booking.Booker#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getBooker__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see Hotel.Booking.Booker#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getBooker__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see Hotel.Booking.Booker#confirmBooking(int)
	 * @generated
	 */
	EOperation getBooker__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see Hotel.Booking.Booker#initiateCheckout(int)
	 * @generated
	 */
	EOperation getBooker__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see Hotel.Booking.Booker#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getBooker__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see Hotel.Booking.Booker#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getBooker__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see Hotel.Booking.Booker#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getBooker__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#getBookedRooms(int) <em>Get Booked Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booked Rooms</em>' operation.
	 * @see Hotel.Booking.Booker#getBookedRooms(int)
	 * @generated
	 */
	EOperation getBooker__GetBookedRooms__int();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#getBookingCost(int) <em>Get Booking Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking Cost</em>' operation.
	 * @see Hotel.Booking.Booker#getBookingCost(int)
	 * @generated
	 */
	EOperation getBooker__GetBookingCost__int();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.Booker#getRoomCost(int, int) <em>Get Room Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Cost</em>' operation.
	 * @see Hotel.Booking.Booker#getRoomCost(int, int)
	 * @generated
	 */
	EOperation getBooker__GetRoomCost__int_int();

	/**
	 * Returns the meta object for class '{@link Hotel.Booking.IBookingInterface <em>IBooking Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking Interface</em>'.
	 * @see Hotel.Booking.IBookingInterface
	 * @generated
	 */
	EClass getIBookingInterface();

	/**
	 * Returns the meta object for the reference list '{@link Hotel.Booking.IBookingInterface#getBookings <em>Bookings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Bookings</em>'.
	 * @see Hotel.Booking.IBookingInterface#getBookings()
	 * @see #getIBookingInterface()
	 * @generated
	 */
	EReference getIBookingInterface_Bookings();

	/**
	 * Returns the meta object for the reference '{@link Hotel.Booking.IBookingInterface#getBooker <em>Booker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Booker</em>'.
	 * @see Hotel.Booking.IBookingInterface#getBooker()
	 * @see #getIBookingInterface()
	 * @generated
	 */
	EReference getIBookingInterface_Booker();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.IBookingInterface#clearComponent() <em>Clear Component</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Component</em>' operation.
	 * @see Hotel.Booking.IBookingInterface#clearComponent()
	 * @generated
	 */
	EOperation getIBookingInterface__ClearComponent();

	/**
	 * Returns the meta object for the '{@link Hotel.Booking.IBookingInterface#getBookingFromId(int) <em>Get Booking From Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking From Id</em>' operation.
	 * @see Hotel.Booking.IBookingInterface#getBookingFromId(int)
	 * @generated
	 */
	EOperation getIBookingInterface__GetBookingFromId__int();

	/**
	 * Returns the meta object for class '{@link Hotel.Booking.BookingInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface</em>'.
	 * @see Hotel.Booking.BookingInterface
	 * @generated
	 */
	EClass getBookingInterface();

	/**
	 * Returns the meta object for the reference '{@link Hotel.Booking.BookingInterface#getInstance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Instance</em>'.
	 * @see Hotel.Booking.BookingInterface#getInstance()
	 * @see #getBookingInterface()
	 * @generated
	 */
	EReference getBookingInterface_Instance();

	/**
	 * Returns the meta object for enum '{@link Hotel.Booking.BookingStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see Hotel.Booking.BookingStatus
	 * @generated
	 */
	EEnum getBookingStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BookingFactory getBookingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Hotel.Booking.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Booking.impl.BookingImpl
		 * @see Hotel.Booking.impl.BookingPackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__BOOKING_ID = eINSTANCE.getBooking_BookingId();

		/**
		 * The meta object literal for the '<em><b>Customer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__CUSTOMER = eINSTANCE.getBooking_Customer();

		/**
		 * The meta object literal for the '<em><b>Booking Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__BOOKING_STATUS = eINSTANCE.getBooking_BookingStatus();

		/**
		 * The meta object literal for the '<em><b>Extra Costs</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__EXTRA_COSTS = eINSTANCE.getBooking_ExtraCosts();

		/**
		 * The meta object literal for the '<em><b>Room Type To Integer</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__ROOM_TYPE_TO_INTEGER = eINSTANCE.getBooking_RoomTypeToInteger();

		/**
		 * The meta object literal for the '<em><b>From Day</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__FROM_DAY = eINSTANCE.getBooking_FromDay();

		/**
		 * The meta object literal for the '<em><b>To Day</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__TO_DAY = eINSTANCE.getBooking_ToDay();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__ROOMS = eINSTANCE.getBooking_Rooms();

		/**
		 * The meta object literal for the '<em><b>Room Extras</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__ROOM_EXTRAS = eINSTANCE.getBooking_RoomExtras();

		/**
		 * The meta object literal for the '<em><b>Payment Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__PAYMENT_STATUS = eINSTANCE.getBooking_PaymentStatus();

		/**
		 * The meta object literal for the '<em><b>Is Checked In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___IS_CHECKED_IN = eINSTANCE.getBooking__IsCheckedIn();

		/**
		 * The meta object literal for the '<em><b>Is Checked Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___IS_CHECKED_OUT = eINSTANCE.getBooking__IsCheckedOut();

		/**
		 * The meta object literal for the '<em><b>Is Cancelled</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___IS_CANCELLED = eINSTANCE.getBooking__IsCancelled();

		/**
		 * The meta object literal for the '<em><b>Is Confirmed</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___IS_CONFIRMED = eINSTANCE.getBooking__IsConfirmed();

		/**
		 * The meta object literal for the '<em><b>Is Paid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___IS_PAID = eINSTANCE.getBooking__IsPaid();

		/**
		 * The meta object literal for the '<em><b>Is Unpaid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___IS_UNPAID = eINSTANCE.getBooking__IsUnpaid();

		/**
		 * The meta object literal for the '<em><b>Is Unconfirmed</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___IS_UNCONFIRMED = eINSTANCE.getBooking__IsUnconfirmed();

		/**
		 * The meta object literal for the '{@link Hotel.Booking.impl.BookerImpl <em>Booker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Booking.impl.BookerImpl
		 * @see Hotel.Booking.impl.BookingPackageImpl#getBooker()
		 * @generated
		 */
		EClass BOOKER = eINSTANCE.getBooker();

		/**
		 * The meta object literal for the '<em><b>Current Checkout</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKER__CURRENT_CHECKOUT = eINSTANCE.getBooker_CurrentCheckout();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___LIST_BOOKINGS = eINSTANCE.getBooker__ListBookings();

		/**
		 * The meta object literal for the '<em><b>Edit Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___EDIT_BOOKING__INT_EMAP_DATE_DATE = eINSTANCE.getBooker__EditBooking__int_EMap_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___CANCEL_BOOKING__INT = eINSTANCE.getBooker__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___CHECK_IN_BOOKING__INT = eINSTANCE.getBooker__CheckInBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check Out Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___CHECK_OUT_BOOKING__INT = eINSTANCE.getBooker__CheckOutBooking__int();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___LIST_CHECK_INS__DATE_DATE = eINSTANCE.getBooker__ListCheckIns__Date_Date();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___LIST_CHECK_OUTS__DATE_DATE = eINSTANCE.getBooker__ListCheckOuts__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Make Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE = eINSTANCE.getBooker__MakeBooking__String_String_EMap_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___CHECK_IN_ROOM__STRING_INT = eINSTANCE.getBooker__CheckInRoom__String_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___PAY_ROOM__INT_CREDITCARD = eINSTANCE.getBooker__PayRoom__int_CreditCard();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getBooker__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getBooker__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___CONFIRM_BOOKING__INT = eINSTANCE.getBooker__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___INITIATE_CHECKOUT__INT = eINSTANCE.getBooker__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getBooker__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getBooker__InitiateRoomCheckout__int_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getBooker__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Get Booked Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___GET_BOOKED_ROOMS__INT = eINSTANCE.getBooker__GetBookedRooms__int();

		/**
		 * The meta object literal for the '<em><b>Get Booking Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___GET_BOOKING_COST__INT = eINSTANCE.getBooker__GetBookingCost__int();

		/**
		 * The meta object literal for the '<em><b>Get Room Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKER___GET_ROOM_COST__INT_INT = eINSTANCE.getBooker__GetRoomCost__int_int();

		/**
		 * The meta object literal for the '{@link Hotel.Booking.IBookingInterface <em>IBooking Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Booking.IBookingInterface
		 * @see Hotel.Booking.impl.BookingPackageImpl#getIBookingInterface()
		 * @generated
		 */
		EClass IBOOKING_INTERFACE = eINSTANCE.getIBookingInterface();

		/**
		 * The meta object literal for the '<em><b>Bookings</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IBOOKING_INTERFACE__BOOKINGS = eINSTANCE.getIBookingInterface_Bookings();

		/**
		 * The meta object literal for the '<em><b>Booker</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IBOOKING_INTERFACE__BOOKER = eINSTANCE.getIBookingInterface_Booker();

		/**
		 * The meta object literal for the '<em><b>Clear Component</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_INTERFACE___CLEAR_COMPONENT = eINSTANCE.getIBookingInterface__ClearComponent();

		/**
		 * The meta object literal for the '<em><b>Get Booking From Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_INTERFACE___GET_BOOKING_FROM_ID__INT = eINSTANCE.getIBookingInterface__GetBookingFromId__int();

		/**
		 * The meta object literal for the '{@link Hotel.Booking.impl.BookingInterfaceImpl <em>Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Booking.impl.BookingInterfaceImpl
		 * @see Hotel.Booking.impl.BookingPackageImpl#getBookingInterface()
		 * @generated
		 */
		EClass BOOKING_INTERFACE = eINSTANCE.getBookingInterface();

		/**
		 * The meta object literal for the '<em><b>Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_INTERFACE__INSTANCE = eINSTANCE.getBookingInterface_Instance();

		/**
		 * The meta object literal for the '{@link Hotel.Booking.BookingStatus <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Hotel.Booking.BookingStatus
		 * @see Hotel.Booking.impl.BookingPackageImpl#getBookingStatus()
		 * @generated
		 */
		EEnum BOOKING_STATUS = eINSTANCE.getBookingStatus();

	}

} //BookingPackage
