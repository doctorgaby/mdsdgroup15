/**
 */
package Hotel.Booking.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import Hotel.Booking.Booker;
import Hotel.Booking.Booking;
import Hotel.Booking.BookingInterface;
import Hotel.Booking.BookingPackage;
import Hotel.Booking.IBookingInterface;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Booking.impl.BookingInterfaceImpl#getBookings <em>Bookings</em>}</li>
 *   <li>{@link Hotel.Booking.impl.BookingInterfaceImpl#getBooker <em>Booker</em>}</li>
 *   <li>{@link Hotel.Booking.impl.BookingInterfaceImpl#getInstance <em>Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingInterfaceImpl extends MinimalEObjectImpl.Container implements BookingInterface {
	/**
	 * The cached value of the '{@link #getBookings() <em>Bookings</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookings()
	 * @generated
	 * @ordered
	 */
	protected EList<Booking> bookings;

	/**
	 * The cached value of the '{@link #getBooker() <em>Booker</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBooker()
	 * @generated
	 * @ordered
	 */
	protected Booker booker;

	/**
	 * The cached value of the '{@link #getInstance() <em>Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstance()
	 * @generated NOT
	 * @ordered
	 */
	private static IBookingInterface instance;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private BookingInterfaceImpl() {
		super();
		bookings = new BasicEList<Booking>();
		booker = new BookerImpl();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING_INTERFACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Booking> getBookings() {
		if (bookings == null) {
			bookings = new EObjectResolvingEList<Booking>(Booking.class, this, BookingPackage.BOOKING_INTERFACE__BOOKINGS);
		}
		return bookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Booker getBooker() {
		/*if (booker != null && booker.eIsProxy()) {
			InternalEObject oldBooker = (InternalEObject)booker;
			booker = (Booker)eResolveProxy(oldBooker);
			if (booker != oldBooker) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOKING_INTERFACE__BOOKER, oldBooker, booker));
			}
		}*/
		return booker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Booker basicGetBooker() {
		return booker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBooker(Booker newBooker) {
		Booker oldBooker = booker;
		booker = newBooker;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_INTERFACE__BOOKER, oldBooker, booker));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IBookingInterface getInstance() {
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public static IBookingInterface basicGetInstance() {
		if(instance == null){
			instance = new BookingInterfaceImpl();
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstance(IBookingInterface newInstance) {
		IBookingInterface oldInstance = instance;
		instance = newInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_INTERFACE__INSTANCE, oldInstance, instance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearComponent() {
		bookings.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Booking getBookingFromId(int bookingId) {
		for(Booking booking : bookings){
			if(booking.getBookingId() == bookingId){
				return booking;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING_INTERFACE__BOOKINGS:
				return getBookings();
			case BookingPackage.BOOKING_INTERFACE__BOOKER:
				if (resolve) return getBooker();
				return basicGetBooker();
			case BookingPackage.BOOKING_INTERFACE__INSTANCE:
				if (resolve) return getInstance();
				return basicGetInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING_INTERFACE__BOOKINGS:
				getBookings().clear();
				getBookings().addAll((Collection<? extends Booking>)newValue);
				return;
			case BookingPackage.BOOKING_INTERFACE__BOOKER:
				setBooker((Booker)newValue);
				return;
			case BookingPackage.BOOKING_INTERFACE__INSTANCE:
				setInstance((IBookingInterface)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_INTERFACE__BOOKINGS:
				getBookings().clear();
				return;
			case BookingPackage.BOOKING_INTERFACE__BOOKER:
				setBooker((Booker)null);
				return;
			case BookingPackage.BOOKING_INTERFACE__INSTANCE:
				setInstance((IBookingInterface)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_INTERFACE__BOOKINGS:
				return bookings != null && !bookings.isEmpty();
			case BookingPackage.BOOKING_INTERFACE__BOOKER:
				return booker != null;
			case BookingPackage.BOOKING_INTERFACE__INSTANCE:
				return instance != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING_INTERFACE___CLEAR_COMPONENT:
				clearComponent();
				return null;
			case BookingPackage.BOOKING_INTERFACE___GET_BOOKING_FROM_ID__INT:
				return getBookingFromId((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookingInterfaceImpl
