/**
 */
package Hotel.Booking.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import Hotel.Booking.Booker;
import Hotel.Booking.Booking;
import Hotel.Booking.BookingPackage;
import Hotel.Booking.BookingStatus;
import Hotel.Entities.CreditCard;
import Hotel.Entities.Extra;
import Hotel.Entities.PaymentStatus;
import Hotel.Room.Room;
import Hotel.Room.RoomStatus;
import Hotel.Room.impl.RoomInterfaceImpl;
import Hotel.RoomType.RoomType;
import Hotel.RoomType.impl.RoomTypeInterfaceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Booking.impl.BookerImpl#getCurrentCheckout <em>Current Checkout</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookerImpl extends MinimalEObjectImpl.Container implements Booker {
	/**
	 * The cached value of the '{@link #getCurrentCheckout() <em>Current Checkout</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentCheckout()
	 * @generated
	 * @ordered
	 */
	protected Booking currentCheckout;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Booking getCurrentCheckout() {
		/*if (currentCheckout != null && currentCheckout.eIsProxy()) {
			InternalEObject oldCurrentCheckout = (InternalEObject)currentCheckout;
			currentCheckout = (Booking)eResolveProxy(oldCurrentCheckout);
			if (currentCheckout != oldCurrentCheckout) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOKER__CURRENT_CHECKOUT, oldCurrentCheckout, currentCheckout));
			}
		}*/
		return currentCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Booking basicGetCurrentCheckout() {
		return currentCheckout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentCheckout(Booking newCurrentCheckout) {
		Booking oldCurrentCheckout = currentCheckout;
		currentCheckout = newCurrentCheckout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKER__CURRENT_CHECKOUT, oldCurrentCheckout, currentCheckout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return 
	 * @generated NOT
	 */
	public EList<Booking> listBookings() {
		EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
		for(Booking booking : bookings){
			if(booking.isConfirmed()){
				List<Room> rooms = booking.getRooms();
				Map<RoomType,Integer> roomTypeAmounts = new HashMap<>();
				for(Room room : rooms){
					if(roomTypeAmounts.containsKey(room.getRoomType())){
						roomTypeAmounts.put(room.getRoomType(), roomTypeAmounts.get(room.getRoomType())+1);
					} else {
						roomTypeAmounts.put(room.getRoomType(), 1);
					}
				}
				System.out.print("[Booking ID: " + booking.getBookingId() + ", Room Types: ");
				for(Map.Entry<RoomType, Integer> entry : roomTypeAmounts.entrySet()){
					System.out.print(entry.getKey().getDescription() + " (" + entry.getValue() + "), ");
				}
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
				System.out.print(format.format(booking.getFromDay()));
				System.out.print("-" + format.format(booking.getToDay()) + "]\n");
			}
		}
		return bookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return 
	 * @generated NOT
	 */
	public boolean editBooking(int bookingId, EMap<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		
		if (roomTypeToInteger == null){
			return false;
		}
		if(booking != null){
			if (fromDay.getTime() < toDay.getTime() && fromDay.getYear() <= toDay.getYear()) {
				booking.setFromDay(fromDay);
				booking.setToDay(toDay);
				booking.setRoomTypeToInteger((EMap<RoomType, Integer>) roomTypeToInteger);
				return true;
			} else {
				System.out.println("Invalid date!");
				return false;
			}
		} else {
			System.out.println("A booking with ID " + bookingId + " does not exist!");
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingId) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		if(booking != null){
			booking.setBookingStatus(BookingStatus.CANCELLED);
			for(Room room : booking.getRooms()){
				room.setRoomStatus(RoomStatus.FREE);
			}
			return true;
		} else {
			System.out.println("A booking with ID " + bookingId + " does not exist!");
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInBooking(int bookingId) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		if(booking != null){
			booking.setBookingStatus(BookingStatus.CHECKED_IN);
			List<Room> bookedRooms = BookingInterfaceImpl.basicGetInstance().getBooker().getBookedRooms(bookingId);
			for(Room room : bookedRooms){
				RoomInterfaceImpl.basicGetInstance().getRoomClass().checkInRoom(booking.getBookingId(), room.getRoomType().getDescription());
			}
			return true;
		} else {
			System.out.println("A booking with ID " + bookingId + " does not exist!");
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutBooking(int bookingId) {
		if(currentCheckout == null) {
			Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
			if(booking != null){
				booking.setBookingStatus(BookingStatus.CONFIRMED);
				currentCheckout = booking;
				return true;
			} else {
				System.out.println("A booking with ID " + bookingId + " does not exist!");
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listCheckIns(Date fromDay, Date toDay) {
		EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
		EList<Booking> checkIns = new BasicEList<Booking>();

 		for(Booking booking : bookings) {
			if((booking.getFromDay().after(fromDay) || booking.getFromDay().equals(fromDay)) && (booking.getFromDay().before(toDay) || booking.getFromDay().equals(toDay))) {
				checkIns.add(booking);
 			}
 		}

		return checkIns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listCheckOuts(Date fromDay, Date toDay) {
		EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
		EList<Booking> checkOuts = new BasicEList<Booking>();

		for(Booking booking : bookings) {
			if((booking.getToDay().after(fromDay) || booking.getToDay().equals(fromDay)) && (booking.getToDay().before(toDay) || booking.getToDay().equals(toDay))) {
				checkOuts.add(booking);
			}
		}
		return checkOuts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int makeBooking(String firstName, String lastName,  EMap<RoomType, Integer> roomTypeToInteger, Date fromDay, Date toDay) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String from = format.format(fromDay);
		String to = format.format(toDay);
		
		//Initiate booking
		int bookingId = initiateBooking(firstName, from, to, lastName);

		//Book rooms
		for(Map.Entry<RoomType, Integer> entry : roomTypeToInteger){
			for(int i = 0; i < entry.getValue(); i++){
				addRoomToBooking(entry.getKey().getDescription(), bookingId);
			}
		}
		
		//Confirm booking
		confirmBooking(bookingId);
		
		return bookingId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingId) {
		return RoomInterfaceImpl.basicGetInstance().getRoomClass().checkInRoom(bookingId, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoom(int roomId, CreditCard creditCard) {
		
		double price = 0;
		try {			
			se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires bank = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();

			if(currentCheckout != null && currentCheckout.getBookingStatus() == BookingStatus.CONFIRMED) {
				for(Room room : currentCheckout.getRooms()) {
					if (room != null){
						room.setPaymentStatus(PaymentStatus.UNPAID);
						price += room.getRoomType().getPrice();
						if(currentCheckout.getRoomExtras().get(room) != null){
							for (Extra extra : currentCheckout.getRoomExtras().get(room)){
								price += extra.getPrice();
							}
						}
					}
				}
				if (bank.makePayment(creditCard.getCcNumber(), creditCard.getCcv(), creditCard.getExpiryMonth(), creditCard.getExpiryYear(), creditCard.getFirstName(), creditCard.getLastName(), price)){
					currentCheckout.setBookingStatus(BookingStatus.CHECKED_OUT);
					return true;
				}
				currentCheckout.getRooms().clear();
			}
			
			BookingInterfaceImpl.basicGetInstance().getBooker().setCurrentCheckout(null);
		} catch(SOAPException e) {
			return false;
		}

		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date from = null;
		Date to = null;

		try {
			from = format.parse(startDate);
			to = format.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if(firstName == null || lastName == null || from == null || to == null || firstName.isEmpty() || lastName.isEmpty() || from.after(to)){
			return -1; //Alternative flow (2.1)
		} else {
			Booking booking = BookingFactoryImpl.init().createBooking();
			EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
			int bookingId = bookings.size()+1;
			booking.setBookingId(bookingId);
			booking.setPaymentStatus(PaymentStatus.UNPAID);
			booking.setBookingStatus(BookingStatus.UNCONFIRMED);
			booking.setFromDay(from);
			booking.setToDay(to);
			bookings.add(booking);
			return bookingId;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID);
		RoomType roomType = RoomTypeInterfaceImpl.basicGetInstance().getRoomTypeFromDescription(roomTypeDescription);
		if(roomType != null && booking != null){
			if (booking.getBookingStatus() != null && booking.getPaymentStatus().equals(PaymentStatus.UNPAID)) {
				//Get free rooms during the booking time period
				EMap<RoomType, EList<Room>> roomTypeRoomMap = RoomInterfaceImpl.basicGetInstance().getRoomLister().searchFreeRooms(booking.getFromDay(), booking.getToDay(),0);
				EList<Room> rooms = roomTypeRoomMap.get(roomType);	//List of rooms of the provided room type
				if(rooms != null && !rooms.isEmpty()){
					rooms.get(0).setRoomStatus(RoomStatus.BOOKED); //Take the first free room
					booking.getRooms().add(rooms.get(0));
					return true;
				}
			}
		}
		System.out.println("The provided booking or room type does not exist!");
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID);
		if(booking != null){
			for(Room room : booking.getRooms()){
				if(room.getRoomStatus().equals(RoomStatus.BOOKED)){
					booking.setBookingStatus(BookingStatus.CONFIRMED);
					return true;
				} 
			}
			System.out.println("Booking with ID " + bookingID + " has no booked rooms.");
		} else {
			System.out.println("No booking with ID " + bookingID + " exists!");
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingID);
		
		if(booking != null){
			if(BookingInterfaceImpl.basicGetInstance().getBooker().checkOutBooking(bookingID)) {
				
				double sum = 0;
				for(Room room : booking.getRooms()) { // For all the rooms in the booking
					room.setPaymentStatus(PaymentStatus.UNPAID); // Should this be UNPAID? i.e look at iniateBooking()
					sum += getRoomCost(bookingID, room.getRoomID());; // Add the price of the room to the totalprice
				}

				return sum;
			}
		}
		System.out.println("No booking with ID " + bookingID + " exists!");
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {

		
		Booking currentCheckout = BookingInterfaceImpl.basicGetInstance().getBooker().getCurrentCheckout();

		double price = 0;
		try {			
			se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires bank = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();

			if(currentCheckout != null && currentCheckout.getPaymentStatus().equals(PaymentStatus.UNPAID)) {
				for(Room room : currentCheckout.getRooms()) {
					if (room != null){
						room.setPaymentStatus(PaymentStatus.UNPAID);
						price += getRoomCost(currentCheckout.getBookingId(), room.getRoomID());
					}
				}
				if (bank.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, price)){
					currentCheckout.setPaymentStatus(PaymentStatus.PAID);
					BookingInterfaceImpl.basicGetInstance().getBooker().setCurrentCheckout(null);
					return true;
				}
			}
		} catch(SOAPException e) {
			return false;
		}

		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingId) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);

		if(booking != null){
			Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomNumber);
			if (room != null && booking.getRooms().contains(room)){
				if (room.isOccupied()){
					room.setPaymentStatus(PaymentStatus.UNPAID);
					return getRoomCost(bookingId, roomNumber);
				}
			}
		}
		System.out.println("Initiate room checkout for room " + roomNumber + " was not successful!");
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {

		try {			
			se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires bank = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
			
			EList<Booking> bookings = BookingInterfaceImpl.basicGetInstance().getBookings();
			for (Booking booking : bookings){
				for(Room room : booking.getRooms()) {
					if (room.getRoomID() == roomNumber && room.getPaymentStatus().equals(PaymentStatus.UNPAID)){
						double price = getRoomCost(booking.getBookingId(), roomNumber);
						if (bank.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, price)){
							room.setRoomStatus(RoomStatus.FREE);
							BookingInterfaceImpl.basicGetInstance().getBooker().setCurrentCheckout(null);
							return true;
						}
					}
				}
			}
			
		} catch(SOAPException e) {
			return false;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Room> getBookedRooms(int bookingId) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		EList<Room> bookedRooms = new BasicEList<Room>();
		if(booking != null){
			for(Room room : booking.getRooms()){
				if(room.isBooked()){
					bookedRooms.add(room);
				}
			}
		}
		return bookedRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double getBookingCost(int bookingId) {
		double cost = 0;
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		if(booking != null){
			for(Room room : booking.getRooms()){
				cost += room.getRoomType().getPrice();
				EList<Extra> roomExtras = booking.getRoomExtras().get(room);
				if(roomExtras != null){
					for(Extra extra : roomExtras){
						cost += extra.getPrice();
					}
				}
			}
			return cost;
		} else {
			return 0;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double getRoomCost(int bookingId, int roomId) {
		Booking booking = BookingInterfaceImpl.basicGetInstance().getBookingFromId(bookingId);
		Room room = RoomInterfaceImpl.basicGetInstance().getRoomFromId(roomId);
		if(booking != null && room != null){
			double cost = room.getRoomType().getPrice();
			EList<Extra> roomExtras = booking.getRoomExtras().get(room);
			if(roomExtras != null){
				for(Extra extra : roomExtras){
					cost += extra.getPrice();
				}
			}
			return cost;
		}
		return 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKER__CURRENT_CHECKOUT:
				if (resolve) return getCurrentCheckout();
				return basicGetCurrentCheckout();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKER__CURRENT_CHECKOUT:
				setCurrentCheckout((Booking)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKER__CURRENT_CHECKOUT:
				setCurrentCheckout((Booking)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKER__CURRENT_CHECKOUT:
				return currentCheckout != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKER___LIST_BOOKINGS:
				return listBookings();
			case BookingPackage.BOOKER___EDIT_BOOKING__INT_EMAP_DATE_DATE:
				return editBooking((Integer)arguments.get(0), (EMap<RoomType, Integer>)arguments.get(1), (Date)arguments.get(2), (Date)arguments.get(3));
			case BookingPackage.BOOKER___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKER___CHECK_IN_BOOKING__INT:
				return checkInBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKER___CHECK_OUT_BOOKING__INT:
				return checkOutBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKER___LIST_CHECK_INS__DATE_DATE:
				return listCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case BookingPackage.BOOKER___LIST_CHECK_OUTS__DATE_DATE:
				return listCheckOuts((Date)arguments.get(0), (Date)arguments.get(1));
			case BookingPackage.BOOKER___MAKE_BOOKING__STRING_STRING_EMAP_DATE_DATE:
				return makeBooking((String)arguments.get(0), (String)arguments.get(1), (EMap<RoomType, Integer>)arguments.get(2), (Date)arguments.get(3), (Date)arguments.get(4));
			case BookingPackage.BOOKER___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKER___PAY_ROOM__INT_CREDITCARD:
				return payRoom((Integer)arguments.get(0), (CreditCard)arguments.get(1));
			case BookingPackage.BOOKER___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case BookingPackage.BOOKER___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKER___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case BookingPackage.BOOKER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case BookingPackage.BOOKER___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case BookingPackage.BOOKER___GET_BOOKED_ROOMS__INT:
				return getBookedRooms((Integer)arguments.get(0));
			case BookingPackage.BOOKER___GET_BOOKING_COST__INT:
				return getBookingCost((Integer)arguments.get(0));
			case BookingPackage.BOOKER___GET_ROOM_COST__INT_INT:
				return getRoomCost((Integer)arguments.get(0), (Integer)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookerImpl