/**
 */
package Hotel.Entities;

import Hotel.RoomType.RoomType;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Type To Integer Map Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Entities.RoomTypeToIntegerMapEntry#getKey <em>Key</em>}</li>
 *   <li>{@link Hotel.Entities.RoomTypeToIntegerMapEntry#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see Hotel.Entities.EntitiesPackage#getRoomTypeToIntegerMapEntry()
 * @model
 * @generated
 */
public interface RoomTypeToIntegerMapEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(RoomType)
	 * @see Hotel.Entities.EntitiesPackage#getRoomTypeToIntegerMapEntry_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomType getKey();

	/**
	 * Sets the value of the '{@link Hotel.Entities.RoomTypeToIntegerMapEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(RoomType value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Integer)
	 * @see Hotel.Entities.EntitiesPackage#getRoomTypeToIntegerMapEntry_Value()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Integer getValue();

	/**
	 * Sets the value of the '{@link Hotel.Entities.RoomTypeToIntegerMapEntry#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Integer value);

} // RoomTypeToIntegerMapEntry
