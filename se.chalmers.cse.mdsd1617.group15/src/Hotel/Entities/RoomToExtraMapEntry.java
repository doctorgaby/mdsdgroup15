/**
 */
package Hotel.Entities;

import Hotel.Room.Room;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room To Extra Map Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Entities.RoomToExtraMapEntry#getKey <em>Key</em>}</li>
 *   <li>{@link Hotel.Entities.RoomToExtraMapEntry#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see Hotel.Entities.EntitiesPackage#getRoomToExtraMapEntry()
 * @model
 * @generated
 */
public interface RoomToExtraMapEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(Room)
	 * @see Hotel.Entities.EntitiesPackage#getRoomToExtraMapEntry_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Room getKey();

	/**
	 * Sets the value of the '{@link Hotel.Entities.RoomToExtraMapEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(Room value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference list.
	 * The list contents are of type {@link Hotel.Entities.Extra}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference list.
	 * @see Hotel.Entities.EntitiesPackage#getRoomToExtraMapEntry_Value()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Extra> getValue();

} // RoomToExtraMapEntry
