/**
 */
package Hotel.Entities;

import Hotel.Room.Room;

import Hotel.RoomType.RoomType;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Type To Room Map Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Entities.RoomTypeToRoomMapEntry#getKey <em>Key</em>}</li>
 *   <li>{@link Hotel.Entities.RoomTypeToRoomMapEntry#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see Hotel.Entities.EntitiesPackage#getRoomTypeToRoomMapEntry()
 * @model
 * @generated
 */
public interface RoomTypeToRoomMapEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(RoomType)
	 * @see Hotel.Entities.EntitiesPackage#getRoomTypeToRoomMapEntry_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomType getKey();

	/**
	 * Sets the value of the '{@link Hotel.Entities.RoomTypeToRoomMapEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(RoomType value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference list.
	 * The list contents are of type {@link Hotel.Room.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference list.
	 * @see Hotel.Entities.EntitiesPackage#getRoomTypeToRoomMapEntry_Value()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Room> getValue();

} // RoomTypeToRoomMapEntry
