/**
 */
package Hotel.Entities;

import Hotel.Booking.Booking;

import Hotel.Room.Room;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking To Room Map Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Hotel.Entities.BookingToRoomMapEntry#getKey <em>Key</em>}</li>
 *   <li>{@link Hotel.Entities.BookingToRoomMapEntry#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see Hotel.Entities.EntitiesPackage#getBookingToRoomMapEntry()
 * @model
 * @generated
 */
public interface BookingToRoomMapEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(Booking)
	 * @see Hotel.Entities.EntitiesPackage#getBookingToRoomMapEntry_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Booking getKey();

	/**
	 * Sets the value of the '{@link Hotel.Entities.BookingToRoomMapEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(Booking value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference.
	 * @see #setValue(Room)
	 * @see Hotel.Entities.EntitiesPackage#getBookingToRoomMapEntry_Value()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Room getValue();

	/**
	 * Sets the value of the '{@link Hotel.Entities.BookingToRoomMapEntry#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Room value);

} // BookingToRoomMapEntry
