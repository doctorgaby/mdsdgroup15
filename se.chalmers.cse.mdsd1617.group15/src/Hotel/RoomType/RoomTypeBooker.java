/**
 */
package Hotel.RoomType;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booker</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Hotel.RoomType.RoomTypePackage#getRoomTypeBooker()
 * @model
 * @generated
 */
public interface RoomTypeBooker extends EObject {

} // RoomTypeBooker
