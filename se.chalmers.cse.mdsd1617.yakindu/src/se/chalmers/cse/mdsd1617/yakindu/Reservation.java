package se.chalmers.cse.mdsd1617.yakindu;

import se.chalmers.cse.mdsd1617.yakindu.Room;

public class Reservation {
	private long reservationId;
	private Room room;
	private ReservationStatus status;
	
	public enum ReservationStatus {
		IDLE,CREATED,CONFIRMED,CHECKED_IN,CHECKED_OUT
	}
	
	public Reservation(long reservationId){
		this.reservationId = reservationId;
		status = ReservationStatus.IDLE;
	}
	
	public void setRoom(Room room){
		this.room = room;
	}
	
	public long getReservationId(){
		return reservationId;
	}
	
	public Room getRoom(){
		return room;
	}
	
	public void setStatus(ReservationStatus status){
		this.status = status;
	}
	
	public boolean isConfirmed(){
		return status.equals(ReservationStatus.CONFIRMED);
	}
	
	public boolean isCheckedIn(){
		return status.equals(ReservationStatus.CHECKED_IN);
	}

	public boolean isCheckedOut() {
		return status.equals(ReservationStatus.CHECKED_OUT);
	}
		
}
